import routes from "routers/index"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import "./App.scss";

const history = createBrowserHistory();

function App() {
  return (
    <Router history={history}>
      <Switch>
        {routes.map((route, index) => (
          <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.component}
          />
        ))}
      </Switch>
    </Router>
  );
}

export default App;
