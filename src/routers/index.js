import LandingPage from "pages/LandingPage";
import AboutUs from "pages/AboutUs";
import OurPrograms from "pages/OurPrograms";
import Contact from "pages/Contact";
import NewsDetail from "pages/news/NewsDetail";
import NewsList from "pages/news/NewsList";
import News from "pages/news/News";
import SignUp from "pages/SignUp";
import SignIn from "pages/SignIn";
import Logout from "pages/Logout";
import LocalHeroes from "pages/registration/LocalHeroes";
import IntHeroes from "pages/registration/IntHeroes";
import Member from "pages/registration/Member";
import EditProfile from "pages/EditProfile";
import Profile from "pages/Profile";
import ChangePassword from "pages/ChangePassword";
import VerifikasiAkun from "pages/verifikasiAkun/VerifikasiAkun";
import VerifikasiRequest from "pages/verifikasiAkun/VerifikasiRequest";
import ForgotPassRequest from "pages/forgotPass/ForgotPassRequest";
import ResetPassword from "pages/forgotPass/ResetPassword";
import Career from "pages/Career";

const routes = [{
  path: "/",
  exact: true,
  component: LandingPage
},
{
  path: "/about-us",
  component: AboutUs
},
{
  path: "/our-programs",
  component: OurPrograms
},
{
  path: "/news/detail/:id",
  component: NewsDetail
},
{
  path: "/news/list",
  component: NewsList
},
{
  path: "/news",
  component: News
},
{
  path: "/contact",
  component: Contact
},
{
  path: "/sign-up",
  component: SignUp
},
{
  path: "/sign-in",
  component: SignIn
},
{
  path: "/logout",
  component: Logout
},
{
  path: "/registration/local-heroes",
  component: LocalHeroes
},
{
  path: "/registration/int-heroes",
  component: IntHeroes
},
{
  path: "/registration/member",
  component: Member
},
{
  path: "/profile/edit-profile",
  component: EditProfile
},
{
  path: "/profile/change-password",
  component: ChangePassword
},
{
  path: "/profile",
  component: Profile
},
{
  path: "/verifikasi-akun/:token",
  component: VerifikasiAkun
},
{
  path: "/verifikasi-request",
  component: VerifikasiRequest
},
{
  path: "/forgot-password",
  component: ForgotPassRequest
},
{
  path: "/reset-password/:token",
  component: ResetPassword
},
{
  path: "/career",
  component: Career
}
];

export default routes