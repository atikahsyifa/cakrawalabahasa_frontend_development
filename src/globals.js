import axios from "axios";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3500,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", window.$sweetAlert.stopTimer);
      toast.addEventListener("mouseleave", window.$sweetAlert.resumeTimer);
    },
  });

const globalData = function () {
    window.$urlPublic = process.env.REACT_APP_PUBLIC
    window.$urlPublicUser = process.env.REACT_APP_PUBLIC_USER
    window.$urlApi = process.env.REACT_APP_API
    window.$axios = axios
    window.$sweetAlert = Swal
    window.$toast = Toast
}

export default globalData