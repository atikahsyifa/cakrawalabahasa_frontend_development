import {post} from "adapters/xhr";

export default function sendMessage(someUrl, payload) {
  return post(someUrl, payload);
}