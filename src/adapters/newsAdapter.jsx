import get, {post} from "adapters/xhr";

export default function getArtikelLatest(someUrl) {
  return get(someUrl);
}

export function getArtikelFeatured(someUrl) {
  return get(someUrl);
}

export function getDoumentasi(someUrl) {
  return get(someUrl);
}

export function getArtikelKategori(someUrl) {
  return get(someUrl);
}

export function getArtikel(someUrl) {
  return get(someUrl);
}

export function getArtikelPopuler(someUrl) {
  return get(someUrl);
}

export function searchArtikel(someUrl, payload) {
  return post(someUrl, payload);
}

export function sendArtikelsComment(someUrl, payload) {
  return post(someUrl, payload);
}

export function getArtikelsComment(someUrl) {
  return get(someUrl);
}

export function likeComment(someUrl, payload) {
  return post(someUrl, payload);
}

export function likeBalasComment(someUrl, payload) {
  return post(someUrl, payload);
}

export function sendBalasComment(someUrl, payload) {
  return post(someUrl, payload);
}

export function reportComment(someUrl, payload) {
  return post(someUrl, payload);
}