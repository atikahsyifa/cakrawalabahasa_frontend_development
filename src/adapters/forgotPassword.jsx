import get, {post} from "adapters/xhr";

export default function resetPassword(someUrl, payload) {
  return post(someUrl, payload);
}

export function kirimEmailForgotPass(someUrl, payload) {
  return post(someUrl, payload);
}

export function checkToken(someUrl, payload) {
  return post(someUrl, payload);
}