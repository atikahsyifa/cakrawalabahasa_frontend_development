import {post} from "adapters/xhr";

export default function sendProfile(someUrl, payload) {
  return post(someUrl, payload);
}