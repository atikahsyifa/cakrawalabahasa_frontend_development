import {post} from "adapters/xhr";

export default function submitForm(someUrl, payload) {
  return post(someUrl, payload);
}