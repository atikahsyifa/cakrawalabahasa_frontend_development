import {put, post} from "adapters/xhr";

export default function submitProfile(someUrl, payload) {
  return put(someUrl, payload);
}

export function changePassword(someUrl, payload) {
  return post(someUrl, payload);
}