import get, {post} from "adapters/xhr";

export default function verifikasiAkun(someUrl) {
  return get(someUrl);
}

export function kirimEmailVerifikasiAkun(someUrl, payload) {
  return post(someUrl, payload);
}