import { post } from "adapters/xhr";

export default function searchArtikelNavbar(someUrl, payload) {
  return post(someUrl, payload);
}