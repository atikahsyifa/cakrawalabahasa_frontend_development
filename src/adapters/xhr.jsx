import Axios from "axios";
import getToken from "store/storageToken";

Axios.defaults.baseURL = process.env.REACT_APP_API
if (getToken() !== undefined && getToken() !== null) Axios.defaults.headers.common.Authorization = getToken()

export default function get(url){
  return Axios.get(url);
}

export function post(url, requestData){
  return Axios.post(url, requestData);
}

export function put(url, requestData){
  return Axios.put(url, requestData);
}

export function deletes(url){
  return Axios.delete(url);
}