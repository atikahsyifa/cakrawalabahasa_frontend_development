import React, { Component } from 'react'
import { Redirect, Link } from "react-router-dom";
import { Form, Button } from "react-bootstrap";

import resetPassword ,{ checkToken } from 'adapters/forgotPassword'

export default class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        password: "",
        password_confirmation: "",
      },
      redirect: false,
      token: this.props.match.params.token,
      errorForm: {},
      busy: "d-none",
      validated: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.checkToken();
  }

  async checkToken() {
    try {
      const payload = {
        token: this.state.token
      }
      await checkToken('checktoken-resetpass', payload)
        .then((resp) => {
          window.$toast.fire({
            icon: "success",
            title: resp.data.message,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
        this.setState({
          redirect: true,
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    this.setState({
      redirect: true,
    });
  }

  async submitForm() {
    try {
      this.setState({ errorForm: {}, validated: "", busy: "d-block" });
      const payload = this.state.form;
      payload['token'] = this.state.token
      await resetPassword('reset-password', payload)
        .then((response) => {
          window.$toast.fire({
            icon: "success",
            title: response.data.message,
          });
          this.setState({
            redirect: true,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
          this.setState({
            errorForm: error.response.data.errors || error.response.data.message,
            validated: false,
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    this.setState({ busy: "d-none" });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    let formTemp = { ...this.state.form };
    formTemp[target.name] = value;
    this.setState({
      form: { ...formTemp },
    });
  }

  handleSubmit(event) {
    this.submitForm();
    event.preventDefault();
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/sign-in" />;
    }

    let errorPassword;
    if (this.state.errorForm.password) {
      errorPassword = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.password}
        </Form.Control.Feedback>
      );
    }

    let errorPassword_confirmation;
    if (this.state.errorForm.password_confirmation) {
      errorPassword_confirmation = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.password_confirmation}
        </Form.Control.Feedback>
      );
    }

    return (
      <>
        <div className="col-md-6 col-lg-4 offset-md-3 offset-lg-4 p-5">
          <h2 className="text-bold">Masukkan Password Baru</h2>
          <Form validated={this.state.validated} onSubmit={this.handleSubmit} className="my-3">
            <Form.Group hasValidation controlId="password" className="mb-5">
              <Form.Label>Password</Form.Label>
              <Form.Control
                required
                name="password"
                type="password"
                placeholder="Enter Password Here"
                value={this.state.form.password}
                onChange={this.handleInputChange}
                isInvalid ={errorPassword}
              />
              {errorPassword}
            </Form.Group>

            <Form.Group hasValidation controlId="password_confirmation" className="mb-5">
              <Form.Label>Password Confirm</Form.Label>
              <Form.Control
                required
                name="password_confirmation"
                type="password"
                placeholder="Enter Password Confirm Here"
                value={this.state.form.password_confirmation}
                onChange={this.handleInputChange}
                isInvalid ={errorPassword_confirmation}
              />
              {errorPassword_confirmation}
            </Form.Group>

            <Button
              variant="warning"
              type="submit"
              className="text-light mt-5 m-auto d-flex text-center w-100"
            >
              <span
                className={`spinner-border text-light ${this.state.busy}`}
                role="status"
                aria-hidden="true"
                style={{ marginRight: "20px" }}
              ></span>
              <span className="m-auto">Reset Password</span>
            </Button>
          </Form>
          <small>Sudah punya akun ? <Link to="/sign-in"> Sign In </Link></small>
        </div>
      </>
    )
  }
}
