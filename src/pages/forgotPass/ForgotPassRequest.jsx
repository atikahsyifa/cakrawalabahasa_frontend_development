import React, { Component } from 'react'
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { kirimEmailForgotPass } from 'adapters/forgotPassword'

export default class VerifikasiRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        email: "",
      },
      errorForm: {},
      busy: "d-none",
      validated: "",
      redirect: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async submitForm() {
    try {
      this.setState({ errorForm: {}, validated: "", busy: "d-block" });
      const payload = this.state.form;
      await kirimEmailForgotPass('kirim-email-lupa-password', payload)
        .then((response) => {
          window.$toast.fire({
            icon: "success",
            title: response.data.message,
          });
          this.setState({
            redirect: true,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
          this.setState({
            errorForm: error.response.data.errors || error.response.data.message,
            validated: false,
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    this.setState({ busy: "d-none" });
  }

  handleInputChange(event) {
    const value = event.target.value;
    let formTemp = { ...this.state.form };
    formTemp['email'] = value;
    this.setState({
      form: { ...formTemp },
    });
  }

  handleSubmit(event) {
    this.submitForm();
    event.preventDefault();
  }

  render() {
    let errorEmail;
    if (this.state.errorForm.email) {
      errorEmail = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.email}
        </Form.Control.Feedback>
      );
    }

    return (
      <>
        <div className="col-md-6 col-lg-4 offset-md-3 offset-lg-4 p-5">
          <h2 className="text-bold">Kirim Email Reset Password</h2>
          <Form validated={this.state.validated} onSubmit={this.handleSubmit} className="my-3">
            <Form.Group hasValidation controlId="email" className="mb-5">
              <Form.Label>Email</Form.Label>
              <Form.Control
                required
                name="email"
                type="email"
                placeholder="Enter Email Here"
                value={this.state.form.email}
                onChange={this.handleInputChange}
                isInvalid ={errorEmail}
              />
              {errorEmail}
            </Form.Group>
            <Button
              variant="warning"
              type="submit"
              className="text-light mt-5 m-auto d-flex text-center w-100"
            >
              <span
                className={`spinner-border text-light ${this.state.busy}`}
                role="status"
                aria-hidden="true"
                style={{ marginRight: "20px" }}
              ></span>
              <span className="m-auto">Kirim</span>
            </Button>
          </Form>
          <small>Sudah punya akun ? <Link to="/sign-in"> Sign In </Link></small>
        </div>
      </>
    )
  }
}
