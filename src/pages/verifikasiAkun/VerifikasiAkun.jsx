import React, { Component } from 'react'
import { Redirect } from "react-router-dom";

import verifikasiAkun from 'adapters/verifikasiAkunAdaptor'

export default class VerifikasiAkun extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      token: this.props.match.params.token
    };
  }

  componentDidMount() {
    this.verifikasiAkun();
  }


  async verifikasiAkun() {
    try {
      await verifikasiAkun(`verifikasi-akun/${this.state.token}`)
        .then((resp) => {
          window.$toast.fire({
            icon: "success",
            title: resp.data.message,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    this.setState({
      redirect: true,
    });
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/sign-in" />;
    }

    return (
      <>

      </>
    )
  }
}
