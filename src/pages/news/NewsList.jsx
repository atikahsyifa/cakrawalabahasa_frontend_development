import React, { Component } from "react";
import Pagination from "react-js-pagination";

import {
  Button
} from "react-bootstrap";

import { Link } from "react-router-dom";
import { faChevronRight, faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Header from "components/parts/Header";
import Footer from "components/parts/Footer";
import HorizontalNewsCard from "components/_module/horizontalNewsCard";
import TitleNewsLink from "components/_module/titleNewsLink";
import { searchArtikel, getArtikelPopuler, getArtikelKategori } from 'adapters/newsAdapter'

import { Helmet } from "react-helmet";
import LogoBrand from "assets/images/logo/logoBrand.jpg";
import styleNewsList from "styles/newsList.module.css";

export default class NewsList extends Component {
  constructor(props) {
    super(props);
    const queryParams = this.props.location.search
    this.state = {
      artikelPopulers: [],
      artikels: [],
      kategoriId: queryParams.split("=")[2] || null,
      kategori: "",
      perPage: 10,
      currentPage: 1,
      totalItem: 0,
      artikelKategori: []
    };
  }

  componentDidMount() {
    this.getArtikel();
    this.getArtikelPopuler();
    this.getArtikelKategori();
  }

  UNSAFE_componentWillMount() {
    this.props.history.listen(async (location) => {
      await this.setState({
        kategoriId: location.search.split("=")[2],
        perPage: 10,
        currentPage: 1,
        totalItem: 0
      });
      this.getArtikel();
      this.getArtikelKategori();
    });
  }

  async handlePageChange(pageNumber) {
    await this.setState({
      currentPage: pageNumber,
    });
    this.getArtikel();
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }

  async getArtikelKategori() {
    try {
      await getArtikelKategori("artikel/artikel-kategori")
        .then((resp) => {
          const kategori = resp.data.artikel_kategori.filter((kategori) => parseInt(kategori.id) === parseInt(this.state.kategoriId))
          this.setState({
            artikelKategori: resp.data.artikel_kategori,
            kategori: kategori[0].artikel_kategori
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }

  async getArtikel() {
    try {
      const payload = {
        per_page: this.state.perPage,
        current_page: this.state.currentPage,
        kategori: this.state.kategoriId
      }
      await searchArtikel("artikel/artikel-list", payload)
        .then((resp) => {
          let datas = resp.data.artikels;
          datas.map((data, index) => {
            datas[index].linkJudul = data.judul.replace(/ /g, "-");
            datas[index].dateUpload = data.created_at.split("T")[0];
            datas[index].timeUpload = data.created_at
              .split("T")[1]
              .split(".000000Z")[0];
          });
          this.setState({
            artikels: datas,
            perPage: resp.data.per_page,
            currentPage: resp.data.current_page,
            totalItem: resp.data.total,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }

  async getArtikelPopuler() {
    try {
      await getArtikelPopuler("artikel/artikel-populer")
        .then((resp) => {
          let datas = resp.data.artikels;
          datas.map((data, index) => {
            datas[index].linkJudul = data.judul.replace(/ /g, "-");
            datas[index].dateUpload = data.created_at.split("T")[0];
            datas[index].timeUpload = data.created_at
              .split("T")[1]
              .split(".000000Z")[0];
          });
          this.setState({
            artikelPopulers: datas,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }

  render() {
    return (
      <>
        <Header {...this.props}></Header>

        <Helmet>
          <title>{this.state.kategori.charAt(0).toUpperCase() + this.state.kategori.slice(1)} | Kursus Bahasa dengan Native Speakers Online</title>
          <meta name="description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
          <meta name="keywords" content="cakrawala bahasa, kursus bahasa, kelas bahasa, kursus bahasa online, kelas bahasa online, kursus bahasa asing, native speakers, platform bahasa, belajar bahasa, belajar bahasa daerah, belajar bahasa asing online" />
          <link rel="canonical" href={`https://cakrawalabahasa.com/news/list?kategori=${this.state.kategori.charAt(0).toUpperCase() + this.state.kategori.slice(1)}&id=${this.state.kategoriId}`} />

          <meta property="og:title" content={`${this.state.kategori.charAt(0).toUpperCase() + this.state.kategori.slice(1)} | Kursus Bahasa dengan Native Speakers Online`} />
          <meta property="og:description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
          <meta property="og:image" content={LogoBrand} />

          <meta name="twitter:title" content={`${this.state.kategori.charAt(0).toUpperCase() + this.state.kategori.slice(1)} | Kursus Bahasa dengan Native Speakers Online`} />
          <meta name="twitter:description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
          <meta name="twitter:image" content={LogoBrand} />
        </Helmet>

        <div id="news-list">
          <div className="container mt-3">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li>
                  <Link to="/">
                    <FontAwesomeIcon icon={faHome} />
                  </Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  <Link to="/news">News</Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  {this.state.kategori.charAt(0).toUpperCase() + this.state.kategori.slice(1)}
                </li>
              </ol>
            </nav>

            <div className="row mb-5">
              {this.state.artikelKategori.map((kategori) => (
                <Link to={`/news/list?kategori=${kategori.artikel_kategori}&id=${kategori.id}`} key={kategori.id} className={`${styleNewsList.kategoriLink} p-0`}>
                  <Button variant="outline-warning" size="sm" className={styleNewsList.kategoriButton}
                    active={parseInt(kategori.id) === parseInt(this.state.kategoriId)}
                  >
                    {kategori.artikel_kategori}
                  </Button>
                </Link>
              ))}
            </div>

            <div className="text-center my-5">
              <p>All posts in:</p>
              <h1>{this.state.kategori.charAt(0).toUpperCase() + this.state.kategori.slice(1)}</h1>
            </div>

            <div className="row">
              <div className={`${styleNewsList.newsList} col-md-8 mb-5`}>
                {this.state.artikels.map((artikel) => (
                  <div className="col-12 mb-3" key={artikel.id}>
                    <HorizontalNewsCard
                      imageClass="col-md-5"
                      textClass="col-md-7"
                      artikelData={artikel}
                    />
                  </div>
                ))}
                <div className="col-12 mt-5">
                  <Pagination
                    activePage={this.state.currentPage}
                    itemsCountPerPage={this.state.perPage}
                    totalItemsCount={this.state.totalItem}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange.bind(this)}
                    itemClass="page-item"
                    linkClass="page-link"
                    innerClass="pagination justify-content-center"
                  />
                </div>
              </div>
              <div className={`${styleNewsList.newsList} col-md-4`}>
                <h5 className="mb-5">Artikel Terpopuler</h5>
                {this.state.artikelPopulers.map((populer) => (
                  <TitleNewsLink
                    artikelData={populer}
                    key={populer.id}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>

        <Footer {...this.props}></Footer>
      </>
    );
  }
}
