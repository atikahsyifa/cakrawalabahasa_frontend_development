import React, { Component } from "react";

import {
  Card,
  Carousel,
  Button
} from "react-bootstrap";

import { Link } from "react-router-dom";
import { faChevronRight, faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Header from "components/parts/Header";
import Footer from "components/parts/Footer";
import HorizontalNewsCard from "components/_module/horizontalNewsCard";
import VerticalNewscard from "components/_module/verticalNewscard";
import TitleNewsLink from "components/_module/titleNewsLink";
import getArtikelLatest, { getArtikelFeatured, getDoumentasi, getArtikelKategori } from 'adapters/newsAdapter'

import { Helmet } from "react-helmet";
import LogoBrand from "assets/images/logo/logoBrand.jpg";
import styleNews from "styles/news.module.css";

function CarouselItem(props) {
  let dokumentasi = props.dokumentasi;
  const items = dokumentasi.filter((dokumen, index) => index < 5).map((dokumen, index) =>
    index === 0
      ?
      <div className="col-6 mb-3" key={index}>
        <div className="col-md-12 container">
          <Card.Img
            variant="top"
            src={`${window.$urlPublic}/img/dokumentasi/${dokumen.image}`}
          />
        </div>
      </div>
      :
      <div className="col-3 mb-3" key={index}>
        <div className="col-md-12 container">
          <Card.Img
            variant="top"
            src={`${window.$urlPublic}/img/dokumentasi/${dokumen.image}`}
          />
        </div>
      </div>
  );
  return (
    items
  );
}

export default class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artikelKategori: [],
      artikelFeatureds: [],
      artikelLatests: [],
      dokumentasis: [],
    };
  }

  componentDidMount() {
    this.getArtikelKategori();
    this.getArtikelFeatured();
    this.getDoumentasi();
    this.getArtikelLatest();
  }

  async getArtikelKategori() {
    try {
      await getArtikelKategori("artikel/artikel-kategori")
        .then((resp) => {
          this.setState({
            artikelKategori: resp.data.artikel_kategori,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }

  async getArtikelFeatured() {
    try {
      await getArtikelFeatured("artikel/artikel-featured")
        .then((resp) => {
          let datas = resp.data.artikel_featureds;
          datas.map((data, index) => {
            datas[index].artikel.linkJudul = data.artikel.judul.replace(/ /g, "-");
            datas[index].artikel.dateUpload = data.artikel.created_at.split("T")[0];
            datas[index].artikel.timeUpload = data.artikel.created_at
              .split("T")[1]
              .split(".000000Z")[0];
          });
          this.setState({
            artikelFeatureds: datas,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }

  async getArtikelLatest() {
    try {
      await getArtikelLatest("artikel/artikel")
        .then((resp) => {
          let datas = resp.data.artikels;
          datas.map((data, index) => {
            datas[index].linkJudul = data.judul.replace(/ /g, "-");
            datas[index].dateUpload = data.created_at.split("T")[0];
            datas[index].timeUpload = data.created_at
              .split("T")[1]
              .split(".000000Z")[0];
          });
          this.setState({
            artikelLatests: datas,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }

  async getDoumentasi() {
    try {
      await getDoumentasi("dokumentasi")
        .then((resp) => {
          let datas = resp.data.dokuemntasi_batchs;
          this.setState({
            dokumentasis: datas,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }

  render() {
    return (
      <>
        <Header {...this.props}></Header>

        <Helmet>
          <title>News | Kursus Bahasa dengan Native Speakers Online</title>
          <meta name="description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
          <meta name="keywords" content="cakrawala bahasa, kursus bahasa, kelas bahasa, kursus bahasa online, kelas bahasa online, kursus bahasa asing, native speakers, platform bahasa, belajar bahasa, belajar bahasa daerah, belajar bahasa asing online" />
          <link rel="canonical" href="https://cakrawalabahasa.com/news" />

          <meta property="og:title" content="News | Kursus Bahasa dengan Native Speakers Online" />
          <meta property="og:description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
          <meta property="og:image" content={LogoBrand} />

          <meta name="twitter:title" content="News | Kursus Bahasa dengan Native Speakers Online" />
          <meta name="twitter:description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
          <meta name="twitter:image" content={LogoBrand} />
        </Helmet>

        <div id="news-channel">
          <div className="container mt-3">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li>
                  <Link to="/">
                    <FontAwesomeIcon icon={faHome} />
                  </Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  <p>News</p>
                </li>
              </ol>
            </nav>
          </div>

          <div className="container">
            <h1 className="text-bold">News Channel</h1>
            <div className="row mb-5">
              {this.state.artikelKategori.map((kategori) => (
                <Link to={`/news/list?kategori=${kategori.artikel_kategori}&id=${kategori.id}`} key={kategori.id} className={`${styleNews.kategoriLink} p-0`}>
                  <Button variant="outline-warning" size="sm" className={styleNews.kategoriButton}>
                    {kategori.artikel_kategori}
                  </Button>
                </Link>
              ))}
            </div>

            <Card>
              <Card.Body>
                <h2 className="text-bold">Latest News</h2>
                <div className={`${styleNews.newsList} row`}>
                  {this.state.artikelLatests.map((artikelLatest, index) => (
                    index === 0
                      ?
                      <div className="col-12 mb-3" key={artikelLatest.id}>
                        <HorizontalNewsCard
                          imageClass="col-md-7"
                          textClass="col-md-5"
                          artikelData={artikelLatest}
                        />
                      </div>
                      :
                      <div className="col-md-4 mb-3" key={artikelLatest.id}>
                        <VerticalNewscard
                          artikelData={artikelLatest}
                        />
                      </div>
                  ))}
                </div>
              </Card.Body>
            </Card>

            {
              this.state.artikelFeatureds.length > 7 &&
              <Card className="my-5">
                <Card.Body>
                  <h2 className="text-bold">Featured</h2>
                  <div className="row">
                    <div className="col-md-8">
                      <div className={`${styleNews.newsList} row`}>
                        {this.state.artikelFeatureds.map((featured, index) => (
                          index < 4 &&
                          <div className="col-md-6 mb-3" key={featured.artikel.id}>
                            <VerticalNewscard
                              artikelData={featured.artikel}
                            />
                          </div>
                        ))}
                      </div>
                    </div>
                    <div className="col-md-4">
                      {this.state.artikelFeatureds.map((featured, index) => (
                        index >= 4 &&
                        <TitleNewsLink
                          artikelData={featured.artikel}
                          key={featured.artikel.id}
                        />
                      ))}
                    </div>
                  </div>
                </Card.Body>
              </Card>
            }
          </div>

          {
            this.state.dokumentasis.length > 1 &&
            <div className={`${styleNews.dokumentasi} py-3 my-5`}>
              <div className="container">
                <h2 className="my-4 text-light">Dokumentations</h2>
                <Carousel>
                  {this.state.dokumentasis.map((dokumentasi) => (
                    <Carousel.Item key={dokumentasi.id}>
                      <h5 className="text-bold text-light">{dokumentasi.judul}</h5>
                      <div className="row">
                        <CarouselItem dokumentasi={dokumentasi.dokumentasi} />
                      </div>
                    </Carousel.Item>
                  ))}
                </Carousel>
              </div>
            </div>
          }
        </div>

        <Footer {...this.props}></Footer>
      </>
    )
  }
}
