import React, { Component } from "react";

import { Link } from "react-router-dom";
import { faChevronRight, faHome, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Header from "components/parts/Header";
import Footer from "components/parts/Footer";
import CommentComponent from "components/newsDetailComponent/commentComponent/CommentComponent";
import TitleNewsLink from "components/_module/titleNewsLink";
import { getArtikel, getArtikelPopuler } from 'adapters/newsAdapter'
import { Helmet } from "react-helmet";

import FB from "assets/images/newsDetail/facebook.png";
import Share from "assets/images/newsDetail/share.png";
import Twitter from "assets/images/newsDetail/twitter.png";
import WA from "assets/images/newsDetail/whatsapp.png";

import styleNewsDetail from "styles/newsDetail.module.css";
import LogoBrand from "assets/images/logo/logoBrand.jpg";

export default class NewsDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artikelPopulers: [],
      artikel: "",
      kategori: "",
      id: this.props.match.params.id,
      dateUpload: "",
      timeUpload: "",
      tag: "",
      text_lead: ""
    };
  }

  componentDidMount() {
    this.getArtikel();
    this.getArtikelPopuler();
  }

  UNSAFE_componentWillMount() {
    try {
      this.props.history.listen(async (location) => {
        await this.setState({
          id: location.pathname.split("/")[3],
        });
        this.getArtikel();
        this.getArtikelPopuler();
      });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }

  async getArtikel() {
    try {
      await getArtikel(`artikel/artikel/${this.state.id}`)
        .then((resp) => {
          const artikel = resp.data.artikel
          this.setState({
            artikel: artikel,
            dateUpload: artikel.created_at.split("T")[0],
            timeUpload: artikel.created_at
              .split("T")[1]
              .split(".000000Z")[0],
            kategori: artikel.artikel_kategori
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }

  async getArtikelPopuler() {
    try {
      await getArtikelPopuler("artikel/artikel-populer")
        .then((resp) => {
          let datas = resp.data.artikels;
          datas.map((data, index) => {
            datas[index].linkJudul = data.judul.replace(/ /g, "-");
            datas[index].dateUpload = data.created_at.split("T")[0];
            datas[index].timeUpload = data.created_at
              .split("T")[1]
              .split(".000000Z")[0];
          });
          this.setState({
            artikelPopulers: datas,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }

  render() {
    return (
      <>
        <Header {...this.props}></Header>

        <Helmet>
          <title>Kontak Kami | Cakrawala Bahasa</title>
          <meta name="description" content={`${this.state.artikel.text_lead} | Cakrawala Bahasa`} />
          <meta name="keywords" content={this.state.artikel.meta_tag} />
          <link rel="canonical" href={`https://cakrawalabahasa.com/news/detail/${this.state.artikel.id}/${this.state.artikel.linkJudul}`} />

          <meta property="og:title" content={`${this.state.artikel.judul} | Cakrawala Bahasa`} />
          <meta property="og:description" content={`${this.state.artikel.text_lead} | Cakrawala Bahasa`} />
          <meta property="og:image" content={LogoBrand} />

          <meta name="twitter:title" content={`${this.state.artikel.judul} | Cakrawala Bahasa`} />
          <meta name="twitter:description" content={`${this.state.artikel.text_lead} | Cakrawala Bahasa`} />
          <meta name="twitter:image" content={LogoBrand} />
        </Helmet>

        <div id="news-detail">
          <div className="container mt-3">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li>
                  <Link to="/">
                    <FontAwesomeIcon icon={faHome} />
                  </Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  <Link to="/news">News</Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  <Link to={`/news/list?kategori=${this.state.kategori.artikel_kategori}&id=${this.state.kategori.id}`}>
                    {this.state.kategori.artikel_kategori}
                  </Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  {this.state.artikel.judul}
                </li>
              </ol>
            </nav>
          </div>

          <div className="container">

            <div className="row">
              <div className="col-md-8">
                <div className="row mb-3">
                  <h2 className={styleNewsDetail.title}>{this.state.artikel.judul}</h2>
                </div>
                <div className={`${styleNewsDetail.shareButton} row`}>
                  <img src={Share} className="img-fluid" alt="share" />
                  <a
                    target="blank"
                    href={`https://www.facebook.com/sharer/sharer.php?u=${window.location.href}`}
                    className="fb-xfbml-parse-ignore"
                  >
                    <img src={FB} className="img-fluid" alt="facebook share" />
                  </a>
                  <a
                    target="blank"
                    href={`https://twitter.com/share?ref_src=${window.location.href}`}
                    data-show-count="false"
                  >
                    <img src={Twitter} className="img-fluid" alt="twitter share" />
                  </a>
                  <a href={`whatsapp://send?text=${window.location.href}`} data-action="share/whatsapp/share">
                    <img src={WA} className="img-fluid" alt="whatsapp share" />
                  </a>
                </div>

                <div className="row mt-4 mb-3">
                  <img
                    src={
                      `${window.$urlPublic}/img/artikel/${this.state.artikel.image}`
                    }
                    className="img-fluid"
                    alt="Contact Us"
                  />
                </div>
                <p className={`${styleNewsDetail.author} my-0`}><FontAwesomeIcon icon={faUser} />  {this.state.artikel.author}</p>
                <small className={`${styleNewsDetail.small} font-size-sm`}>
                  {this.state.dateUpload} {this.state.timeUpload}
                </small>

                <div className="row my-3">
                  <div
                    dangerouslySetInnerHTML={{ __html: this.state.artikel.artikel }}
                  />
                </div>

                <div className={`${styleNewsDetail.shareButton} row`}>
                  <img src={Share} className="img-fluid" alt="share" />
                  <a
                    target="blank"
                    href={`https://www.facebook.com/sharer/sharer.php?u=${window.location.href}`}
                    className="fb-xfbml-parse-ignore"
                  >
                    <img src={FB} className="img-fluid" alt="facebook share" />
                  </a>
                  <a
                    target="blank"
                    href="https://twitter.com/share?ref_src=https://news.detik.com"
                    data-show-count="false"
                  >
                    <img src={Twitter} className="img-fluid" alt="twitter share" />
                  </a>
                  <a href={`whatsapp://send?text=${window.location.href}`} data-action="share/whatsapp/share">
                    <img src={WA} className="img-fluid" alt="whatsapp share" />
                  </a>
                </div>
              </div>

              <div className="col-md-4 mt-md-0 mt-5">
                <h5 className="mb-5">Artikel Terpopuler</h5>
                {this.state.artikelPopulers.map((populer) => (
                  <TitleNewsLink
                    artikelData={populer}
                    key={populer.id}
                  />
                ))}
              </div>
            </div>

            <div className="col-md-8">
              <hr className="my-5" />
              <h4 className="text-bold">Comment</h4>
              <CommentComponent idArtikel={this.state.id} />
            </div>

          </div>
        </div>

        <Footer {...this.props}></Footer>
      </>
    );
  }
}
