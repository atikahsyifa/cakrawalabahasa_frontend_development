import React, { Component } from "react";
import { Link } from "react-router-dom";
import { faChevronRight, faHeadphonesAlt, faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Container, Row, Col, FormLabel, FormGroup, ToggleButton, ToggleButtonGroup, FormControl } from "react-bootstrap";
import { ButtonGroup, ButtonToolbar, Form, Button } from "react-bootstrap";
import sendMessage from 'adapters/careerAdapter'

import Header from "components/parts/Header";
import Footer from "components/parts/Footer";

//import ContactImage from "assets/images/contact/contactImage.webp";

import styleCareer from "styles/career.module.css";
import 'react-phone-number-input/style.css';

import { Helmet } from "react-helmet";
import LogoBrand from "assets/images/logo/logoBrand.jpg";
import undraw from "assets/images/career/undraw.png";
import undraw1 from "assets/images/career/undraw1.png";
import undraw2 from "assets/images/career/undraw2.png";
import undraw3 from "assets/images/career/undraw3.png";
import undraw4 from "assets/images/career/undraw4.png";
import undraw5 from "assets/images/career/undraw5.png";
import undraw6 from "assets/images/career/undraw6.png";
import PhoneInputWithCountrySelect from "react-phone-number-input";
import { render } from "react-dom";
//import NumericInput from "react-numeric-input";

export default class Career extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                email: "",
                full_name: "",
                nick_name: "",
                gender: "",
                age: "",
                domicile: "",
                phone_number: "",
                institute: "",
                major: "",
                semester: "",
                intern_info: "",
                why_match: "",
            },
            errorForm: {},
            busy: "d-none",
            validated: "",
            position: "",
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async submitForm() {
        try {
            this.setState({ errorForm: {}, validated: "", busy: "d-block" });
            const payload = this.state.form;
            await sendMessage(`career/apply/${this.state.position}`, payload)
                .then((response) => {
                    window.$toast.fire({
                        icon: "success",
                        title: response.data.message,
                    });
                })
                .catch((error) => {
                    window.$toast.fire({
                        icon: "error",
                        title: error.response.data.message || "Connection Error",
                    });
                    this.setState({
                        validated: false,
                        errorForm: error.response.data.errors,
                    });
                });
        } catch (err) {
            window.$toast.fire({
                icon: "error",
                title: "Connection Error",
            });
        };
        this.setState({ busy: "d-none" });
    }

    handleInputChange(event) {
        const target = event.target;
        const value = event.target.value;
        let formTemp = { ...this.state.form };
        formTemp[target.name] = value;
        this.setState({
            form: { ...formTemp },
        });
    }

    handleSubmit(event) {
        this.submitForm();
        event.preventDefault();
    }

    handlePositionChange(Position) {
        this.setState({
            position: Position,
        })
    }


    card = (props) => {
        const job = props.title;
        let errorEmail;
        let errorFullname;
        let errorNickname;
        let errorGender;
        let errorAge;
        let errorDomicile;
        let errorPhonenumber;
        let errorInstitute;
        let errorMajor;
        let errorSemester;
        let errorInterninfo;
        let errorWhymatch;
        if (this.state.errorForm.email) {
            errorEmail = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.email}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.full_name) {
            errorFullname = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.full_name}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.nick_name) {
            errorNickname = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.nick_name}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.gender) {
            errorGender = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.gender}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.age) {
            errorAge = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.age}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.domicile) {
            errorDomicile = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.domicile}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.phone_number) {
            errorPhonenumber = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.phonenumber}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.institute) {
            errorInstitute = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.institute}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.major) {
            errorMajor = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.major}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.semester) {
            errorSemester = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.semester}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.intern_info) {
            errorInterninfo = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.intern_info}
                </Form.Control.Feedback>
            );
        }
        if (this.state.errorForm.why_match) {
            errorWhymatch = (
                <Form.Control.Feedback type="invalid">
                    {this.state.errorForm.why_match}
                </Form.Control.Feedback>
            );
        }
        return (
            <div className="col">
                <div class="card bg-dark text-dark responsiveCard">
                    <img src={props.image} class="card-img" />
                    <div class="card-img-overlay">
                        <div className={styleCareer.cardTitleWrapper}>
                            <h5 class={styleCareer.cardTitle}>{props.title}</h5>
                        </div>
                        <div className={styleCareer.groupsButton}>
                            <ButtonToolbar className={styleCareer.buttons} aria-label="Toolbar with button groups">
                                <ButtonGroup className="me-2" aria-label="First group">
                                    <Button className={styleCareer.bttn} variant="light" data-bs-toggle="modal" data-bs-target="#exampleModal1">Detail</Button>
                                </ButtonGroup>
                                <ButtonGroup className="me-2" aria-label="Second group">
                                    <Button className={styleCareer.bttn2} variant="light" onClick={() => this.handlePositionChange(job)} data-bs-toggle="modal" data-bs-target="#exampleModal">Apply</Button>
                                </ButtonGroup>
                            </ButtonToolbar>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class={styleCareer.modalTitle} id="exampleModalLabel">Detail Vacancy</h2>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                lorem
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class={styleCareer.modalTitle} id="exampleModalLabel">Apply Job</h2>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body mx-5">
                                <Form
                                    validated={this.state.validated}
                                    onSubmit={this.handleSubmit}
                                >
                                    <Form.Group hasValidation controlId="email" className="mb-2 mx-1">
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control
                                            required
                                            name="email"
                                            type="email"
                                            placeholder=""
                                            value={this.state.form.email}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorEmail}
                                        />
                                        {errorEmail}
                                    </Form.Group>
                                    <Form.Group hasValidation className="mb-2 mx-1">
                                        <Form.Label>Fullname</Form.Label>
                                        <Form.Control
                                            required
                                            name="full_name"
                                            type="text"
                                            placeholder=""
                                            value={this.state.form.full_name}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorFullname}
                                        />
                                        {errorFullname}
                                    </Form.Group>
                                    <Form.Group hasValidation className="mb-2 mx-1">
                                        <Form.Label>Nickname</Form.Label>
                                        <Form.Control
                                            required
                                            name="nick_name"
                                            type="text"
                                            placeholder=""
                                            value={this.state.form.nick_name}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorNickname}
                                        />
                                        {errorNickname}
                                    </Form.Group>
                                    <ToggleButtonGroup name="gender" type="radio" className="mb-2 mx-1">
                                        <FormLabel>Gender</FormLabel>
                                        <div>
                                            <br></br>
                                            <br></br>
                                        </div>
                                        <ToggleButton
                                            required
                                            name="gender"
                                            className={styleCareer.toggleButton}
                                            value="female"
                                            onChange={this.handleInputChange}>
                                            Female
                                        </ToggleButton>
                                        <ToggleButton
                                            required
                                            name="gender"
                                            className={styleCareer.toggleButton}
                                            value="male"
                                            onChange={this.handleInputChange}>
                                            Male
                                        </ToggleButton>
                                        {errorGender}
                                    </ToggleButtonGroup>
                                    <Form.Group hasValidation className="mb-2 mx-1">
                                        <Form.Label>Age</Form.Label>
                                        <Form.Control
                                            required
                                            name="age"
                                            type="number"
                                            placeholder=""
                                            value={this.state.form.age}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorAge}
                                        />
                                        {errorAge}
                                    </Form.Group>
                                    <Form.Group hasValidation className="mb-2 mx-1">
                                        <Form.Label>Domicile</Form.Label>
                                        <Form.Control
                                            required
                                            name="domicile"
                                            type="text"
                                            placeholder=""
                                            value={this.state.form.domicile}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorDomicile}
                                        />
                                        {errorDomicile}
                                    </Form.Group>
                                    <FormGroup hasValidation className="mb-3 mx-1">
                                        <FormLabel>Whatsapp Number</FormLabel>
                                        <FormControl
                                            required
                                            name="phone_number"
                                            type="text"
                                            placeholder=""
                                            value={this.state.form.phone_number}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorPhonenumber}
                                        />
                                        {errorPhonenumber}
                                    </FormGroup>
                                    <Form.Group hasValidation className="mb-3 mx-1">
                                        <Form.Label>University</Form.Label>
                                        <Form.Control
                                            required
                                            name="institute"
                                            type="text"
                                            placeholder=""
                                            value={this.state.form.institute}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorInstitute}
                                        />
                                        {errorInstitute}
                                    </Form.Group>
                                    <Form.Group hasValidation className="mb-3 mx-1">
                                        <Form.Label>Major</Form.Label>
                                        <Form.Control
                                            required
                                            name="major"
                                            type="text"
                                            placeholder=""
                                            value={this.state.form.major}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorMajor}
                                        />
                                        {errorMajor}
                                    </Form.Group>
                                    <Form.Group hasValidation className="mb-3 mx-1">
                                        <Form.Label>Semester</Form.Label>
                                        <Form.Control
                                            required
                                            name="semester"
                                            type="number"
                                            placeholder=""
                                            value={this.state.form.semester}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorSemester}
                                        />
                                        {errorSemester}
                                    </Form.Group>
                                    <Form.Group className="mb-3 mx-1">
                                        <Form.Label>How did you know about this internship information?</Form.Label>
                                        <Form.Control
                                            required
                                            name="intern_info"
                                            as="textarea"
                                            placeholder=""
                                            value={this.state.form.intern_info}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorInterninfo}
                                        />
                                        {errorInterninfo}
                                    </Form.Group>
                                    <Form.Group className="mb-2 mx-1">
                                        <Form.Label>Why are you a perfect match for the role?</Form.Label>
                                        <Form.Control
                                            required
                                            name="why_match"
                                            as="textarea"
                                            placeholder=""
                                            value={this.state.form.why_match}
                                            onChange={this.handleInputChange}
                                            isInvalid={errorWhymatch}
                                        />
                                        {errorWhymatch}
                                    </Form.Group>

                                    <Button
                                        variant="warning"
                                        type="submit"
                                        className="text-light mt-5 m-auto d-flex"
                                        value="submit"
                                    >
                                        <span
                                            className={`spinner-border text-light ${this.state.busy}`}
                                            role="status"
                                            aria-hidden="true"
                                            style={{ marginRight: "20px" }}
                                        ></span>
                                        <span>Send</span>
                                    </Button>
                                </Form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }




    render() {


        return (
            <>
                <Header {...this.props}></Header>
                <Helmet>
                    <title>Career | Cakrawala Bahasa</title>
                    <meta name="description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
                    <meta name="keywords" content="cakrawala bahasa, kursus bahasa, kelas bahasa, kontak kami, kontak cakrawala bahasa, kursus bahasa online, kelas bahasa online, kursus bahasa asing, native speakers, platform bahasa, belajar bahasa, belajar bahasa daerah, belajar bahasa asing online" />
                    <link rel="canonical" href="https://cakrawalabahasa.com/betaversion" />

                    <meta property="og:title" content="Career | Cakrawala Bahasa" />
                    <meta property="og:description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
                    <meta property="og:image" content={LogoBrand} />

                    <meta name="twitter:title" content="Career | Cakrawala Bahasa" />
                    <meta name="twitter:description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
                    <meta name="twitter:image" content={LogoBrand} />

                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
                    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
                </Helmet>
                <div className="container">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li>
                                <Link to="/">
                                    <FontAwesomeIcon icon={faHome} />
                                </Link>
                            </li>
                            <li>
                                <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
                            </li>
                        </ol>
                    </nav>


                    <div className="mx-3">
                        <Container className="py-5 mb-4">
                            <Row>
                                <Col xs={9} md={6} className="pt-5">
                                    <h1 className="fw-bold">Find Your Job Opportunity at PT. Schlemmer Automotive Indonesia</h1>
                                </Col>
                                <Col xs={9} md={6}>
                                    <img src={undraw} className={`${styleCareer.image} img-fluid`} alt="Career" />
                                </Col>
                            </Row>
                        </Container>

                        <div className={styleCareer.fadeInText}>
                            <div className="pb-3 my-5">
                                <div className={styleCareer.wrapper}>
                                    <h2 className={`${styleCareer.bigTitle1} me-2 text-center fw-bold my-3`}>
                                        <span>ABOUT US</span>
                                    </h2>
                                </div>
                                <p className="my-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni voluptatibus earum reiciendis dicta, excepturi est et exercitationem voluptates accusamus. Repudiandae maiores debitis praesentium, obcaecati dolorum alias, quis id reiciendis, culpa blanditiis vero perferendis beatae sapiente tenetur impedit tempore unde aliquam!</p>
                            </div>

                            <div className="my-5">
                                <div className={styleCareer.wrapper}>
                                    <h2 className={`${styleCareer.bigTitle2} me-2 text-center fw-bold my-3`}>
                                        <span>JOB VACANCIES</span>
                                    </h2>
                                </div>
                                <div className="my-5">
                                    <div className="row row-cols-1 row-cols-md-3 g-4 mb-3">
                                        <this.card title="Finance & Accounting" image={undraw}></this.card>
                                        <this.card title="Human Resources & General Affair" image={undraw1}></this.card>
                                        <this.card title="Quality Assurance" image={undraw2}></this.card>
                                    </div>
                                    <div className="row row-cols-1 row-cols-md-3 g-4 mb-3">
                                        <this.card title="Manufacturing" image={undraw3}></this.card>
                                        <this.card title="Research & Development" image={undraw4}></this.card>
                                        <this.card title="Business & Sales" image={undraw5}></this.card>
                                    </div>
                                    <div className={`${styleCareer.lastCard} row row-cols-1 row-cols-md-3 g-4`}>
                                        <this.card title="Supply Chain Management" image={undraw6}></this.card>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer {...this.props}></Footer>
            </>
        );

    }

}