import React from 'react'
import { Redirect } from "react-router-dom";
import { removeToken } from "store/storageToken";
import { removeData } from "store/storageData";
import Axios from "axios";

export default function Logout() {
  delete Axios.defaults.headers.common.Authorization
  removeToken()
  removeData('user')
  window.$toast.fire({
    icon: "success",
    title: "Anda telah logout",
  });
  return <Redirect to="/" />;
}
