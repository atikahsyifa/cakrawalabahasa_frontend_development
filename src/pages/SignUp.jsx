import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import { Redirect, Link } from "react-router-dom";

import SignUpImage from "assets/images/sign/signUp.webp";

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        name: "",
        username: "",
        phone: "",
        email: "",
        password: "",
        password_confirmation: "",
        agreement: false,
      },
      errorForm: {},
      busy: "d-none",
      validated: "",
      redirect: false,
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async submitForm() {
    try {
      this.setState({ errorForm: {}, validated: "", busy: "d-block" });
      const payload = this.state.form;
      await window.$axios
        .post(`${window.$urlApi}sign-up`, payload)
        .then((response) => {
          window.$toast.fire({
            icon: "success",
            title: response.data.message,
          });
          this.setState({
            redirect: true,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
          this.setState({
            errorForm: error.response.data.errors || error.response.data.message,
            validated: false,
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    this.setState({ busy: "d-none" });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    let formTemp = { ...this.state.form };
    formTemp[target.name] =
      target.name !== "agreement" ? value : event.target.checked;
    this.setState({
      form: { ...formTemp },
    });
  }

  handleSubmit(event) {
    if (!this.state.form.agreement) {
      window.$toast.fire({
        icon: "error",
        title: "You must check The Terms and Privacy Policy",
      });
    } else {
      this.submitForm();
    }
    event.preventDefault();
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/sign-in" />;
    }

    let errorFullName;
    let errorUserName;
    let errorPhone;
    let errorEmail;
    let errorPassword;
    let errorPasswordConfirm;
    let errorAgree;
    if (this.state.errorForm.name) {
      errorFullName = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.name}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.username) {
      errorUserName = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.username}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.phone) {
      errorPhone = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.phone}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.email) {
      errorEmail = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.email}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.password) {
      errorPassword = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.password}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.password_confirmation) {
      errorPasswordConfirm = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.passoword_confirm}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.agreement) {
      errorAgree = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.agreement}
        </Form.Control.Feedback>
      );
    }

    return (
      <>
        <div className="row d-flex align-items-center">
          <div className="col-md-5 d-none d-md-block">
            <img src={SignUpImage} className="img-fluid " alt="signup" />
          </div>
          <div className="col-md-5 offset-md-1 p-5">
            <h1 className="text-bold">Sign Up</h1>
            <Form validated={this.state.validated} onSubmit={this.handleSubmit} className="my-3">
              <Form.Group hasValidation controlId="name" className="mb-5">
                <Form.Label>Full Name</Form.Label>
                <Form.Control
                  required
                  name="name"
                  type="text"
                  placeholder="Enter Text Here"
                  value={this.state.form.name}
                  onChange={this.handleInputChange}
                  isInvalid ={errorFullName}
                />
                {errorFullName}
              </Form.Group>
              <div className="row">
                <div className="col-md-6">
                  <Form.Group hasValidation controlId="username" className="mb-5">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                      required
                      name="username"
                      type="text"
                      placeholder="Enter Text Here"
                      value={this.state.form.username}
                      onChange={this.handleInputChange}
                      isInvalid ={errorUserName}
                    />
                    {errorUserName}
                  </Form.Group>
                </div>
                <div className="col-md-6">
                  <Form.Group hasValidation controlId="phone" className="mb-5">
                    <Form.Label>Phone Number</Form.Label>
                    <Form.Control
                      required
                      name="phone"
                      type="number"
                      placeholder="Enter Text Here"
                      value={this.state.form.phone}
                      onChange={this.handleInputChange}
                      isInvalid ={errorPhone}
                    />
                    {errorPhone}
                  </Form.Group>
                </div>
              </div>
              <Form.Group hasValidation controlId="email" className="mb-5">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  required
                  name="email"
                  type="email"
                  placeholder="Enter Text Here"
                  value={this.state.form.email}
                  onChange={this.handleInputChange}
                  isInvalid ={errorEmail}
                />
                {errorEmail}
              </Form.Group>
              <Form.Group hasValidation controlId="password" className="mb-5">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  required
                  name="password"
                  type="password"
                  placeholder="Enter Text Here"
                  value={this.state.form.password}
                  onChange={this.handleInputChange}
                  isInvalid ={errorPassword}
                />
                <small>
                  Password terdiri dari minimal 8 karakter kombinasi huruf
                  besar-kecil dan angka/simbol
                </small>
                {errorPassword}
              </Form.Group>
              <Form.Group hasValidation controlId="password_confirmation" className="mb-5">
                <Form.Label>Password Confirm</Form.Label>
                <Form.Control
                  required
                  name="password_confirmation"
                  type="password"
                  placeholder="Enter Text Here"
                  value={this.state.form.password_confirmation}
                  onChange={this.handleInputChange}
                  isInvalid ={errorPasswordConfirm}
                />
                {errorPasswordConfirm}
              </Form.Group>
              <div className="d-flex align-items-start">
                <input
                type="checkbox"
                name="agreement"
                id="gree"
                className="mr-3"
                checked={this.state.agreement}
                onChange={this.handleInputChange}/>
                <label htmlFor="gree">I agree to The Terms and Privacy Policy.</label>
              </div>
              {errorAgree}
              <Button
                variant="warning"
                type="submit"
                className="text-light mt-5 m-auto d-flex text-center w-100"
              >
                <span
                  className={`spinner-border text-light ${this.state.busy}`}
                  role="status"
                  aria-hidden="true"
                  style={{ marginRight: "20px" }}
                ></span>
                <span className="m-auto">Sign Up</span>
              </Button>
            </Form>
            <small>Sudah punya akun ? <Link to="/sign-in"> Sign In </Link></small>
          </div>
        </div>
      </>
    );
  }
}
