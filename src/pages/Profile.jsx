import React, { Component } from "react";
import { Link } from "react-router-dom";
import { faChevronRight, faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "react-bootstrap";

import Header from "components/parts/Header";
import Footer from "components/parts/Footer";

import ProfileImage from "assets/images/profile/profileImage.jpeg";
import BlankProfile from "assets/images/profile/blankProfile.png";
import styleProfile from"styles/profile.module.css";

import getData from "store/storageData";


export default class ProfileSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {Image_Id: ProfileImage};
    this.userData = getData("user");
  }

  addDefaultSrc(ev){
    ev.target.src = BlankProfile;
  }

  isEmpty(value) {
    if (!value) {
      return value = "-";
    } else {
      return value
    }
  }

  isEmptyToo(valueOne, valueTwo) {
    var value = "";
    if (!valueOne && !valueTwo) {
      return value = "-";
    }
    else if (!valueOne && valueTwo) {
      return valueTwo;
    }
    else if (valueOne && !valueTwo) {
      return valueOne;
    }
    else {
      return value = `${this.userData["tempat_lahir"]}, ${this.userData["tgl_lahir"]}`;
    }
  }


  render() {
    return (
      <>
      <script src="https://unpkg.com/scrollreveal"></script>
      <script src="assets/main.js"></script>
      <Header {...this.props}></Header>

      <div className="container">
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li>
              <Link to="/profile/edit-profile">
                <FontAwesomeIcon icon={faHome} />
              </Link>
            </li>
            <li>
              <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
            </li>
            <li className="breadcrumb-item active" aria-current="page">
              <Link to="/profile">
                Profile
              </Link>
            </li>
          </ol>
        </nav>

        <h1 className="text-bold mb-5">Profile Setting</h1>
      </div>

      <div className={ `${styleProfile.formWrap} mb-3 form-wrap mx-auto`}>
        <div className="container mb-5 d-flex align-items-center">

          <div className="col-md-4 mx-auto">
            <img src={`${window.$urlPublicUser}/img/profile/${this.userData["photo_profile"]}`} onError={this.addDefaultSrc} className="img-fluid rounded-circle" alt="Profile" id="getImage"/>
            <div className="text-decoration-none">
              <div className="btn-group d-flex align-items-center">
                <Link to="/profile/edit-profile">
                  <button  className="btn text-primary pull-left">Change</button>
                </Link>
              </div>
            </div>
          </div>

          <div className="col-md-8 mx-5">
            <h1 className="text-bold">{this.userData["name"]}</h1>
            <p className="mt-2"> @{this.userData["username"]}</p>
          </div>

        </div>
      </div>

        <div className={ `${styleProfile.formWrap} mb-3 form-wrap mx-auto`}>
            
          <div className="col-md-2 ml-5">
            <div className="row mt-3 d-flex align-items-left">

              <h6 className="text-bold text-secondary">Email :</h6>
              <p className="text-secondary mt-2 mx-3">{this.userData["email"]}</p>

            </div>
          </div>

          <div className="row mt-3">

            <h6 className="text-bold text-secondary">Phone :</h6>
            <p className="text-secondary mt-2 mx-3">{this.userData["phone"]}</p>

          </div>

          <div className="row mt-3">

            <h6 className="text-bold text-secondary">Address :</h6>
            <p className="text-secondary mt-2 mx-3">{this.isEmpty(this.userData["alamat"])}</p>

          </div>

          <div className="row mt-3">

            <h6 className="text-bold text-secondary">Birthdate and Place :</h6>
            <p className="text-secondary mt-2 mx-3">{this.isEmptyToo(this.userData["tempat_lahir"], this.userData["tgl_lahir"])}</p>

          </div>

          <div className="row mt-3">

            <h6 className="text-bold text-secondary">Education :</h6>
            <p className="text-secondary mt-2 mx-3">{this.isEmpty(this.userData["pendidikan"]).toUpperCase()}</p>

          </div>

          <div className="d-flex mb-5 justify-content-end">

            <Link to="/profile/edit-profile">
              <Button variant="warning" className="text-light text-center text-bold btn btn-default">
                Edit profile
              </Button>
            </Link>

          </div>

          <hr />

          <div className="row">

            <h6 className="text-bold text-secondary">Password :</h6>
            <p className="text-secondary mt-2 mx-3">**********</p>

          </div>

          <div className="d-flex justify-content-end">

            <Link to="/profile/change-password">
              <Button variant="warning" className="text-light text-center text-bold btn btn-default">
                Edit Password
              </Button>
            </Link>

          </div>

        </div>
    
          <Footer {...this.props}></Footer>

      </> 
    )
  }
}