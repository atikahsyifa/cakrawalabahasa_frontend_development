import React, { Component } from 'react'
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { faChevronRight, faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Header from "components/parts/Header";
import Footer from "components/parts/Footer";
import submitForm from 'adapters/registAdapter';

import Regist from "assets/images/registration/regist.webp";

import styleRegist from"styles/regist.module.css";

export default class IntHeroes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        full_name: "",
        wa_number: "",
        nationality: "",
        age: "",
        status: "",
        language_speake: "",
        language_teach: "",
        division: "",
        follow_ig: "",
        agreement: false,
      },
      errorForm: {},
      busy: "d-none",
      validated: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async submitForm() {
    try {
      this.setState({ errorForm: {}, validated: "", busy: "d-block" });
      const payload = this.state.form;
      await submitForm("regist/int-heroes", payload)
        .then((response) => {
          window.$toast.fire({
            icon: "success",
            title: response.data.message,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
          this.setState({
            errorForm: error.response.data.errors || error.response.data.message,
            validated: false,
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    this.setState({ busy: "d-none" });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    let formTemp = { ...this.state.form };
    formTemp[target.name] =
      target.name !== "agreement" ? value : event.target.checked;
    this.setState({
      form: { ...formTemp },
    });
  }

  handleSubmit(event) {
    if (!this.state.form.agreement) {
      window.$toast.fire({
        icon: "error",
        title: "You must check The Terms and Privacy Policy",
      });
    } else {
      this.submitForm();
    }
    event.preventDefault();
  }

  render() {
    let errorFullName;
    let errorWaNumber;
    let errorAge;
    let errorNationality;
    let errorStatus;
    let errorLanguageSpeake;
    let errorLanguageTeach;
    let errorDivision;
    let errorIg;
    let errorAgree;
    if (this.state.errorForm.full_name) {
      errorFullName = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.full_name}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.wa_number) {
      errorWaNumber = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.wa_number}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.age) {
      errorAge = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.age}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.nationality) {
      errorNationality = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.nationality}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.status) {
      errorStatus = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.status}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.language_speake) {
      errorLanguageSpeake = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.language_speake}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.language_teach) {
      errorLanguageTeach = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.language_teach}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.division) {
      errorDivision = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.division}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.follow_ig) {
      errorIg = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.follow_ig}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.agreement) {
      errorAgree = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.agreement}
        </Form.Control.Feedback>
      );
    }

    
    return (
      <>
        <Header {...this.props}></Header>

        <div id="regist">
          <div
              className={ `${styleRegist.jumbotron} jumbotron d-flex align-items-center` }
              style={{ backgroundImage: `url(${Regist})` }}
            >
            <div className="container text-light">
              <div className="col-lg-8">
                <h1 className="text-bold display-6">Let's Grow With Us</h1>
              </div>
            </div>
          </div>

          <div className="container mt-3">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li>
                  <Link to="/">
                    <FontAwesomeIcon icon={faHome} />
                  </Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  <Link to="/registration">Registration</Link>
                </li>
                <li>
                  <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  Int Heroes
                </li>
              </ol>
            </nav>

            <h1 className="text-bold text-center mb-5">International Heroes Registration</h1>

            <Form validated={this.state.validated} onSubmit={this.handleSubmit} className={ `${styleRegist.formWrap} mb-3 form-wrap mx-auto`}>
              <Form.Group controlId="full_name" className="mb-5">
                <Form.Label>Full Name</Form.Label>
                <Form.Control
                  required
                  name="full_name"
                  type="text"
                  placeholder="Enter Text Here"
                  value={this.state.form.full_name}
                  onChange={this.handleInputChange}
                />
                {errorFullName}
              </Form.Group>
              <div className="row">
                <div className="col-md-6">
                  <Form.Group controlId="wa_number" className="mb-5">
                    <Form.Label>Whatsapp Number</Form.Label>
                    <Form.Control
                      required
                      name="wa_number"
                      type="phone"
                      placeholder="Enter Number Here"
                      value={this.state.form.wa_number}
                      onChange={this.handleInputChange}
                    />
                    {errorWaNumber}
                  </Form.Group>
                </div>
                <div className="col-md-6">
                  <Form.Group controlId="age" className="mb-5">
                    <Form.Label>Age</Form.Label>
                    <Form.Control
                      required
                      name="age"
                      type="number"
                      placeholder="Enter Number Here"
                      value={this.state.form.age}
                      onChange={this.handleInputChange}
                    />
                    {errorAge}
                  </Form.Group>
                </div>
              </div>
              <Form.Group controlId="nationality" className="mb-5">
                <Form.Label>Nationality</Form.Label>
                <Form.Control
                  required
                  name="nationality"
                  type="text"
                  placeholder="Enter Text Here"
                  value={this.state.form.nationality}
                  onChange={this.handleInputChange}
                />
                {errorNationality}
              </Form.Group>
              <Form.Group controlId="status" className="mb-5">
                <Form.Label>Status</Form.Label>
                <Form.Control
                  as="select"
                  required
                  aria-label="Default select example"
                  name="status"
                  value={this.state.form.status}
                  onChange={this.handleInputChange}
                >
                  <option value="" disabled>Select Status</option>
                  <option value="student">Student</option>
                  <option value="employee">Employee</option>
                  <option value="others">Others</option>
                </Form.Control>
                {errorStatus}
              </Form.Group>
              <div className="row">
                <div className="col-md-6">
                  <Form.Group controlId="language_speake" className="mb-5">
                    <Form.Label>Language Speake</Form.Label>
                    <Form.Control
                      required
                      name="language_speake"
                      type="text"
                      placeholder="Enter Text Here"
                      value={this.state.form.language_speake}
                      onChange={this.handleInputChange}
                    />
                    {errorLanguageSpeake}
                  </Form.Group>
                </div>
                <div className="col-md-6">
                  <Form.Group controlId="language_teach" className="mb-5">
                    <Form.Label>Language Teach</Form.Label>
                    <Form.Control
                      required
                      name="language_teach"
                      type="text"
                      placeholder="Enter Text Here"
                      value={this.state.form.language_teach}
                      onChange={this.handleInputChange}
                    />
                    {errorLanguageTeach}
                  </Form.Group>
                </div>
              </div>
              <Form.Group controlId="division" className="mb-5">
                <Form.Label>Division</Form.Label>
                <Form.Control
                  as="select"
                  required
                  aria-label="Default select example"
                  name="division"
                  value={this.state.form.division}
                  onChange={this.handleInputChange}
                >
                  <option value="" disabled>Select Division</option>
                  <option value="orphans">Orphans</option>
                  <option value="local heroes">Local Heroes</option>
                </Form.Control>
                {errorDivision}
              </Form.Group>
              <Form.Group controlId="follow_ig" className="mb-5">
                <Form.Label>Have you followed the Cakrawala Bahasa instagram?</Form.Label>
                <Form.Control
                  as="select"
                  required
                  aria-label="Default select example"
                  name="follow_ig"
                  value={this.state.form.follow_ig}
                  onChange={this.handleInputChange}
                >
                  <option value="n">No</option>
                  <option value="y">Yes</option>
                </Form.Control>
                {errorIg}
              </Form.Group>
              <div className="d-flex align-items-start">
                <input
                type="checkbox"
                checked={this.state.agreement}
                name="agreement"
                id="agree"
                className={ styleRegist.agree }
                onChange={this.handleInputChange}/>
                <label htmlFor="agree">I agree to The Terms and Privacy Policy.</label>
              </div>
              {errorAgree}
              <Button
                variant="warning"
                type="submit"
                className="text-light mt-5 m-auto d-flex text-center w-100"
              >
                <span
                  className={`spinner-border text-light ${this.state.busy}`}
                  role="status"
                  aria-hidden="true"
                  style={{ marginRight: "20px" }}
                ></span>
                <span className="m-auto">Submit</span>
              </Button>
            </Form>
          </div>
        </div>

        <Footer {...this.props}></Footer>
      </> 
    )
  }
}
