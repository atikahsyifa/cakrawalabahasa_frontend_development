import React, { Component } from "react";
import { Link } from "react-router-dom";
import { faChevronRight, faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Card, Form, Button } from "react-bootstrap";
import sendMessage from 'adapters/contactAdapter'

import Header from "components/parts/Header";
import Footer from "components/parts/Footer";

import ContactImage from "assets/images/contact/contactImage.webp";

import styleContact from "styles/contact.module.css";

import { Helmet } from "react-helmet";
import LogoBrand from "assets/images/logo/logoBrand.jpg";

export default class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        nama: "",
        phone: "",
        email: "",
        pesan: "",
        metode: "",
      },
      errorForm: {},
      busy: "d-none",
      validated: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async submitForm() {
    try {
      this.setState({ errorForm: {}, validated: "", busy: "d-block" });
      const payload = this.state.form;
      await sendMessage('contact-us', payload)
        .then((response) => {
          window.$toast.fire({
            icon: "success",
            title: response.data.message,
          });
        })
        .catch((error) => {
          window.$toast.fire({
            icon: "error",
            title: error.response.data.message || "Connection Error",
          });
          this.setState({
            validated: false,
            errorForm: error.response.data.errors,
          });
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    this.setState({ busy: "d-none" });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = event.target.value;
    let formTemp = { ...this.state.form };
    formTemp[target.name] = value;
    this.setState({
      form: { ...formTemp },
    });
  }

  handleSubmit(event) {
    this.submitForm();
    event.preventDefault();
  }

  render() {
    let errorName;
    let errorPhone;
    let errorEmail;
    let errorPesan;
    let errorMetode;
    if (this.state.errorForm.nama) {
      errorName = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.nama}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.phone) {
      errorPhone = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.phone}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.email) {
      errorEmail = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.email}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.pesan) {
      errorPesan = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.pesan}
        </Form.Control.Feedback>
      );
    }
    if (this.state.errorForm.metode) {
      errorMetode = (
        <Form.Control.Feedback type="invalid">
          {this.state.errorForm.metode}
        </Form.Control.Feedback>
      );
    }

    return (
      <>
        <Header {...this.props}></Header>

        <Helmet>
          <title>Kontak Kami | Cakrawala Bahasa</title>
          <meta name="description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
          <meta name="keywords" content="cakrawala bahasa, kursus bahasa, kelas bahasa, kontak kami, kontak cakrawala bahasa, kursus bahasa online, kelas bahasa online, kursus bahasa asing, native speakers, platform bahasa, belajar bahasa, belajar bahasa daerah, belajar bahasa asing online" />
          <link rel="canonical" href="https://cakrawalabahasa.com/contact" />

          <meta property="og:title" content="Kontak Kami | Cakrawala Bahasa" />
          <meta property="og:description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
          <meta property="og:image" content={LogoBrand} />

          <meta name="twitter:title" content="Kontak Kami | Cakrawala Bahasa" />
          <meta name="twitter:description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
          <meta name="twitter:image" content={LogoBrand} />
        </Helmet>

        <div className="container">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li>
                <Link to="/">
                  <FontAwesomeIcon icon={faHome} />
                </Link>
              </li>
              <li>
                <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Contact
              </li>
            </ol>
          </nav>

          <h1>Contact Us</h1>

          <div className="row my-5 d-flex align-items-center">
            <div className="col-md-6">
              <img src={ContactImage} className="img-fluid" alt="Contact Us" />
            </div>
            <div className="col-md-6">
              <Card body>
                <Form
                  validated={this.state.validated}
                  onSubmit={this.handleSubmit}
                >
                  <Form.Group hasValidation controlId="nama" className="mb-5">
                    <Form.Label>Full Name</Form.Label>
                    <Form.Control
                      required
                      name="nama"
                      type="text"
                      placeholder="Enter Text Here"
                      value={this.state.form.nama}
                      onChange={this.handleInputChange}
                      className={styleContact.input}
                      isInvalid={errorName}
                    />
                    {errorName}
                  </Form.Group>
                  <Form.Group hasValidation controlId="phone" className="mb-5">
                    <Form.Label>Phone Number</Form.Label>
                    <Form.Control
                      required
                      name="phone"
                      type="number"
                      placeholder="Enter Text Here"
                      value={this.state.form.phone}
                      onChange={this.handleInputChange}
                      className={styleContact.input}
                      isInvalid={errorPhone}
                    />
                    {errorPhone}
                  </Form.Group>
                  <Form.Group hasValidation controlId="email" className="mb-5">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      required
                      name="email"
                      type="email"
                      placeholder="Enter Text Here"
                      value={this.state.form.email}
                      onChange={this.handleInputChange}
                      className={styleContact.input}
                      isInvalid={errorEmail}
                    />
                    {errorEmail}
                  </Form.Group>
                  <Form.Group hasValidation controlId="pesan" className="mb-5">
                    <Form.Label>Pesan</Form.Label>
                    <Form.Control
                      required
                      name="pesan"
                      as="textarea"
                      placeholder="Enter Text Here"
                      value={this.state.form.pesan}
                      onChange={this.handleInputChange}
                      rows={5}
                      className={styleContact.textarea}
                      isInvalid={errorPesan}
                    />
                    {errorPesan}
                  </Form.Group>
                  <Form.Check
                    label="Hubungi via email"
                    name="metode"
                    type="radio"
                    id="via-email"
                    value="email"
                    onChange={this.handleInputChange}
                  />
                  <Form.Check
                    label="Hubungi via WA"
                    name="metode"
                    type="radio"
                    id="via-wa"
                    value="phone"
                    onChange={this.handleInputChange}
                  />
                  {errorMetode}
                  <Button
                    variant="warning"
                    type="submit"
                    className="text-light mt-5 m-auto d-flex"
                    value="submit"
                  >
                    <span
                      className={`spinner-border text-light ${this.state.busy}`}
                      role="status"
                      aria-hidden="true"
                      style={{ marginRight: "20px" }}
                    ></span>
                    <span>Send Message</span>
                  </Button>
                </Form>
              </Card>
            </div>
          </div>
        </div>

        <Footer {...this.props}></Footer>
      </>
    );
  }
}
