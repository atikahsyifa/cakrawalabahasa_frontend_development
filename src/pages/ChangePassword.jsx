import React, { Component } from 'react'
import { Form, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { faChevronRight, faHome } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Header from 'components/parts/Header'
import Footer from 'components/parts/Footer'
import stylePass from 'styles/passwordProfile.module.css'
import { changePassword } from 'adapters/profileAdapter'

export default class ChangePassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      form: {
        old_password: "",
        new_password: "",
        new_password_confirmation: "",
      },
      errorForm: {},
      busy: 'd-none',
    }
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  async submitForm() {
    try {
      this.setState({ errorForm: {}, busy: 'd-block' })
      const payload = this.state.form
      await changePassword('profile/change-password', payload)
        .then((response) => {
          window.$toast.fire({
            icon: 'success',
            title: response.data.message,
          })
          this.setState({
            redirect: true,
          })
        })
        .catch((error) => {
          window.$toast.fire({
            icon: 'error',
            title: error.response.data.message || 'Connection Error',
          })
          this.setState({
            errorForm:
              error.response.data.errors || error.response.data.message,
          })
        })
    } catch (err) {
      window.$toast.fire({
        icon: 'error',
        title: 'Connection Error',
      })
    }
    this.setState({ busy: 'd-none' })
  }

  handleInputChange(event) {
    const target = event.target
    const value = target.value
    let formTemp = { ...this.state.form }
    formTemp[target.name] = value
    this.setState({
      form: { ...formTemp },
    })
  }

  handleSubmit(event) {
    this.submitForm();
    event.preventDefault()
  }

  render() {
    let errorPassword
    let errorNewPassword
    let errorNewPasswordConfirm
    if (this.state.errorForm.old_password) {
      errorPassword = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.old_password[0]}
        </Form.Control.Feedback>
      )
    }
    if (this.state.errorForm.new_password) {
      errorNewPassword = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.new_password}
        </Form.Control.Feedback>
      )
    }
    if (this.state.errorForm.new_password_confirmation) {
      errorNewPasswordConfirm = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.new_password_confirmation}
        </Form.Control.Feedback>
      )
    }
    return (
      <>
        <Header {...this.props}></Header>
        <div className='container'>
          <nav aria-label='breadcrumb'>
            <ol className='breadcrumb'>
              <li>
                <Link to='/'>
                  <FontAwesomeIcon icon={faHome} />
                </Link>
              </li>
              <li>
                <FontAwesomeIcon icon={faChevronRight} className='mx-3' />
              </li>
              <li className='breadcrumb-item active' aria-current='page'>
                <Link to='/profile'>Profile</Link>
              </li>
              <li>
                <FontAwesomeIcon icon={faChevronRight} className='mx-3' />
              </li>
              <li className='breadcrumb-item active' aria-current='page'>
                Change Password
              </li>
            </ol>
          </nav>
          <h1 className='text-bold text-left mb-5 home__title'>
            Change Password
          </h1>
          <div className='container mt-1'>
            <div className='container mb-5 d-flex'>
              <div className='col-12 mx-auto'>
                <Form
                  onSubmit={this.handleSubmit}
                  className={`${stylePass.formWrap} mb-3 form-wrap mx-auto`}
                >
                  <Form.Group controlId='old-password' hasValidation className='mb-5'>
                    <Form.Label>Old Password</Form.Label>
                    <Form.Control
                      required
                      name='old_password'
                      type='password'
                      placeholder='Enter Your Old Password'
                      value={this.state.form.old_password}
                      onChange={this.handleInputChange}
                      isInvalid ={errorPassword}
                    />
                    {errorPassword}
                  </Form.Group>
                  <Form.Group controlId='password' hasValidation className='mb-5'>
                    <Form.Label>New Password</Form.Label>
                    <Form.Control
                      required
                      name='new_password'
                      type='password'
                      placeholder='Enter Your New Password'
                      value={this.state.form.password}
                      onChange={this.handleInputChange}
                      minLength="6"
                      isInvalid ={errorNewPassword}
                    />
                    <small className="text-primary">Password minimal 6 karakter terdiri dari huruf besar-kecil, angka dan simbol</small>
                    {errorNewPassword}
                  </Form.Group>
                  <Form.Group controlId='password_confirm' hasValidation className='mb-5'>
                    <Form.Label>Confirm New Password</Form.Label>
                    <Form.Control
                      required
                      name='new_password_confirmation'
                      type='password'
                      placeholder='Confirm Your New Password'
                      value={this.state.form.new_password_confirmation}
                      onChange={this.handleInputChange}
                      minLength="6"
                      isInvalid ={errorNewPasswordConfirm}
                    />
                    {errorNewPasswordConfirm}
                  </Form.Group>
                  <div className="d-flex justify-content-end">
                    <Button
                      variant='warning'
                      type='submit'
                      className='text-light text-center mx-2 d-flex'
                    >
                      <span
                        className={`spinner-border text-light ${this.state.busy}`}
                        role='status'
                        aria-hidden=' true'
                        style={{ marginRight: '20px' }}
                      ></span>
                      <span className='m-auto'>Save Changes</span>
                    </Button>

                    <Link to='/profile'>
                      <div className="btn btn-secondary d-inline-block w-auto float-sm-none">
                        cancel
                      </div>
                    </Link>

                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>

        {/* Footer / Halaman Bawah*/}
        <Footer {...this.props}></Footer>
      </>
    )
  }
}
