import React, { Component } from "react";
import SignInFormComponent from "components/signInComponent/SignInFormComponent";
import SignInImageComponent from "components/signInComponent/SignInImageComponent";

export default class signIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      busy: "d-none"
    };
  }

  render() {
    return (
      <>
        <div className="row d-flex align-items-center">

          <SignInImageComponent />
          <SignInFormComponent busy={this.state.busy} />

        </div>
      </>
    );
  }
}
