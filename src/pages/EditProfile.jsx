import React, { Component } from 'react'
import { Form, Button } from 'react-bootstrap'
import DatePicker from 'react-datepicker'
import { Redirect, Link } from 'react-router-dom'
import moment from 'moment';

import { faChevronRight, faHome } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import Header from 'components/parts/Header'
import Footer from 'components/parts/Footer'

import styleRegist from 'styles/editProfile.module.css'
import 'react-datepicker/dist/react-datepicker.css'

import getData, { saveData } from 'store/storageData'
import submitProfile from 'adapters/profileAdapter'

export default class ProfileUser extends Component {
  constructor(props) {
    super(props)
    this.dataUser = getData('user')
    this.state = {
      form: {
        name: this.dataUser.name,
        username: this.dataUser.username,
        email: this.dataUser.email,
        phone: this.dataUser.phone,
        tgl_lahir: this.dataUser.tgl_lahir,
        alamat: this.dataUser.alamat,
        tempat_lahir: this.dataUser.tempat_lahir,
        pendidikan: this.dataUser.pendidikan,
        image: null,
        image_name: null,
      },
      file_name: null,
      imagePreview: `${window.$urlPublicUser}/img/profile/${this.dataUser["photo_profile"]}`,
      errorForm: {},
      busy: 'd-none',
      redirect: false
    }
    this.onImageChange = this.onImageChange.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleDateChange = this.handleDateChange.bind(this)
  }

  async submitForm() {
    try {
      this.setState({ errorForm: {}, busy: 'd-block' })
      const payload = this.state.form
      await submitProfile(`profile/${this.dataUser.id}`, payload)
        .then((response) => {
          const userData = response.data.profile
          saveData('user', userData)
          window.$toast.fire({
            icon: 'success',
            title: response.data.message,
          })
          this.setState({
            redirect: true,
          })
        })
        .catch((error) => {
          window.$toast.fire({
            icon: 'error',
            title: error.response.data.message || 'Connection Error',
          })
          this.setState({
            errorForm:
              error.response.data.errors || error.response.data.message,
          })
        })
    } catch (err) {
      window.$toast.fire({
        icon: 'error',
        title: 'Connection Error',
      })
    }
    this.setState({ busy: 'd-none' })
  }

  handleDateChange(event) {
    let formTemp = { ...this.state.form }
    const dataNewFormat = 
    formTemp['tgl_lahir'] = moment(event).format('YYYY-MM-DD')
    formTemp['tgl_lahir_display'] = event
    this.setState({
      form: { ...formTemp },
    })
  }

  handleInputChange(event) {
    const target = event.target
    const value = target.value
    let formTemp = { ...this.state.form }
    formTemp[target.name] = value
    this.setState({
      form: { ...formTemp },
    })
  }
  
  onImageChange = event => {
    if (event.target.files && event.target.files[0]) {
      const img = event.target.files[0];
      console.log(img)
      if ((img.type !== 'image/jpeg' && img.type !== 'image/png' && img.type !== 'image/jpg') || img.size > 215000) {
        window.$toast.fire({
          icon: "error",
          title: "File harus berformat png/jpg/jpeg dan size tidak lebih dari 215kb",
        });
        event.target.value = null;
        return;
      } 
      
      this.setState({ imagePreview: URL.createObjectURL(img)})
      let formTemp = { ...this.state.form }
      formTemp["image_name"] = img.name

      let reader = new FileReader();
      reader.onload = (e) => {
        formTemp["image"] = e.target.result.replace(/^data:image\/[a-z]+;base64,/, '')
        this.setState({ file_name: e.target.result })

        this.setState({
          form: { ...formTemp },
        });
      };
      reader.readAsDataURL(img);
    }
  };

  handleSubmit(event) {
    if (this.state.file_name !== null && this.state.file_name.split(',')[0] !== 'data:image/png;base64' && this.state.file_name.split(',')[0] !== 'data:image/jpeg;base64' && this.state.file_name.split(',')[0] !== 'data:image/jpg;base64') {
      window.$toast.fire({
        icon: "error",
        title: "File harus berformat gambar",
      });
    } else {
      this.submitForm();
    }
    event.preventDefault();
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/profile" />;
    }

    let errorFullName
    let errorUsername
    let errorEmail
    let errorPhone
    let errorTglLahir
    let errorAlamat
    let errorTempatLahir
    let errorPendidikan
    if (this.state.errorForm.name) {
      errorFullName = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.name}
        </Form.Control.Feedback>
      )
    }
    if (this.state.errorForm.username) {
      errorUsername = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.username}
        </Form.Control.Feedback>
      )
    }
    if (this.state.errorForm.email) {
      errorEmail = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.email}
        </Form.Control.Feedback>
      )
    }
    if (this.state.errorForm.phone) {
      errorPhone = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.phone}
        </Form.Control.Feedback>
      )
    }
    if (this.state.errorForm.tgl_lahir) {
      errorTglLahir = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.tgl_lahir}
        </Form.Control.Feedback>
      )
    }
    if (this.state.errorForm.alamat) {
      errorAlamat = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.alamat}
        </Form.Control.Feedback>
      )
    }
    if (this.state.errorForm.tempat_lahir) {
      errorAlamat = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.tempat_lahir}
        </Form.Control.Feedback>
      )
    }
    if (this.state.errorForm.pendidikan) {
      errorPendidikan = (
        <Form.Control.Feedback type='invalid'>
          {this.state.errorForm.pendidikan}
        </Form.Control.Feedback>
      )
    }

    return (
      <>
        <Header {...this.props}></Header>

        <div className='container'>
          <nav aria-label='breadcrumb'>
            <ol className='breadcrumb'>
              <li>
                <Link to='/'>
                  <FontAwesomeIcon icon={faHome} />
                </Link>
              </li>
              <li>
                <FontAwesomeIcon icon={faChevronRight} className='mx-3' />
              </li>
              <li className='breadcrumb-item active' aria-current='page'>
                <Link to='/profile'>Profile</Link>
              </li>
              <li>
                <FontAwesomeIcon icon={faChevronRight} className='mx-3' />
              </li>
              <li className='breadcrumb-item active' aria-current='page'>
                Change Profile
              </li>
            </ol>
          </nav>
          <h1 className='text-bold text-left mb-5'>Change Profile</h1>
          <div className='container'>
            <Form
              validated={this.state.validated}
              onSubmit={this.handleSubmit}
              className={`${styleRegist.formWrap} mb-3 form-wrap mx-auto`}
            >
              <Form.Group hasValidation controlId='name' className='mb-5'>
                <div className="col-md-4 mx-auto">
                  <img src={this.state.imagePreview} className="img-fluid" alt="Profile" id="getImage"/>
                </div>
                <Form.Label>Photo Profile</Form.Label>
                <Form.File 
                  id="custom-file"
                  custom
                  onChange={this.onImageChange}
                />
                <small className="text-primary">Foto harus berformat png/jpg/jpeg, size tidak lebih dari 215kb</small>
              </Form.Group>
              <Form.Group hasValidation controlId='name' className='mb-5'>
                <Form.Label>Full Name</Form.Label>
                <Form.Control
                  required
                  name='name'
                  type='text'
                  placeholder='Enter Text Here'
                  value={this.state.form.name}
                  onChange={this.handleInputChange}
                  isInvalid ={errorFullName}
                />
                {errorFullName}
              </Form.Group>
              <Form.Group hasValidation controlId='username' className='mb-5'>
                <Form.Label>Username</Form.Label>
                <Form.Control
                  required
                  name='username'
                  type='text'
                  placeholder='Enter Your New Username'
                  value={this.state.form.username}
                  onChange={this.handleInputChange}
                  isInvalid ={errorUsername}
                />
                {errorUsername}
              </Form.Group>
              <Form.Group hasValidation controlId='email' className='mb-5'>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  required
                  name='email'
                  type='email'
                  placeholder='Enter Your New Email'
                  value={this.state.form.email}
                  onChange={this.handleInputChange}
                  isInvalid ={errorEmail}
                />
                {errorEmail}
              </Form.Group>
              <Form.Group hasValidation controlId='phone' className='mb-5'>
                <Form.Label>Phone</Form.Label>
                <Form.Control
                  required
                  name='phone'
                  type='text'
                  placeholder='Enter Number Here'
                  value={this.state.form.phone}
                  onChange={this.handleInputChange}
                  isInvalid ={errorPhone}
                />
                {errorPhone}
              </Form.Group>
              <div className='row'>
                <div className='col-md-6'>
                  <Form.Group hasValidation controlId='tempat_lahir' className='mb-5'>
                    <Form.Label>Place</Form.Label>
                    <Form.Control
                      required
                      name='tempat_lahir'
                      type='text'
                      placeholder='Enter Text Here'
                      value={this.state.form.tempat_lahir}
                      onChange={this.handleInputChange}
                      isInvalid ={errorTempatLahir}
                    />
                    {errorTempatLahir}
                  </Form.Group>
                </div>
                <div className='col-md-6'>
                  <Form.Group hasValidation controlId='tgl_lahir' className='mb-5'>
                    <Form.Label>Birthdate</Form.Label>
                    <br />
                    <DatePicker
                      name='tgl_lahir'
                      selected={this.state.form.tgl_lahir_display}
                      value={this.state.form.tgl_lahir_display}
                      onChange={this.handleDateChange}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode='select'
                      className='form-control'
                      placeholderText={moment(this.state.form.tgl_lahir).format('l')}
                    />
                    {errorTglLahir}
                  </Form.Group>
                </div>
              </div>
              <Form.Group hasValidation className='mb-5' controlId='alamat'>
                <Form.Label>Address</Form.Label>
                <Form.Control
                  as='textarea'
                  required
                  name='alamat'
                  type='text'
                  value={this.state.form.alamat}
                  onChange={this.handleInputChange}
                  isInvalid ={errorAlamat}
                />
                {errorAlamat}
              </Form.Group>
              <Form.Group hasValidation controlId='pendidikan' className='mb-5'>
                <Form.Label>Education</Form.Label>
                <Form.Control
                  as='select'
                  required
                  aria-label='Default select example'
                  name='pendidikan'
                  value={this.state.form.pendidikan}
                  onChange={this.handleInputChange}
                  isInvalid ={errorPendidikan}
                >
                  <option value='null'>
                    Select Last Education
                  </option>
                  <option value='sd'>SD</option>
                  <option value='smp'>SMP</option>
                  <option value='sma'>SMA</option>
                  <option value='s1'>D3/S1</option>
                  <option value='s2'>S2</option>
                  <option value='s3'>S3</option>
                </Form.Control>
                {errorPendidikan}
              </Form.Group>

              <div className="d-flex justify-content-end">

                <Button
                  variant='warning'
                  type='submit'
                  className='text-light text-center mx-2 d-flex'
                >
                  <span
                    className={`spinner-border text-light ${this.state.busy}`}
                    role='status'
                    aria-hidden=' true'
                    style={{ marginRight: '20px' }}
                  ></span>
                  <span className='m-auto'>Save Changes</span>
                </Button>

                <Link to='/profile'>
                  <div className="btn btn-secondary d-inline-block w-auto float-sm-none">
                    cancel
                  </div>
                </Link>

              </div>
            </Form>
          </div>
        </div>

        <Footer {...this.props}></Footer>
      </>
    )
  }
}
