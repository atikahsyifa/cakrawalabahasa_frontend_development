import React, { Component } from "react";
import { Link } from "react-router-dom";
import { faChevronRight, faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Header from "components/parts/Header";
import Footer from "components/parts/Footer";

import Program25Bahasa from "assets/images/ourPrograms/25bahasa.webp";
import PrivateClass from "assets/images/ourPrograms/privateClass.webp";
import MahirBahasa from "assets/images/ourPrograms/mahirBahasa.webp";
import ForSchool from "assets/images/ourPrograms/forSchool.webp";
import Penerjemah from "assets/images/ourPrograms/penerjemah.webp";
import styleOurPrograms from "styles/ourPrograms.module.css";

import {Helmet} from "react-helmet";
import LogoBrand from "assets/images/logo/logoBrand.jpg";

export default class OurProgram extends Component {
  constructor(props) {
    super(props);
    this.state = [
      {
        title: "Kelas 25 Bahasa",
        description: `Kelas pembelajaran yang menyediakan 25 pilihan bahasa sesuai dengan silabus dan modul pembelajaran 
        yang sudah disediakan. Satu kelas akan diisi 15-30 siswa. Pembelajaran akan diadakan dalam 8 pertemuan selama satu bulan.`,
        image: Program25Bahasa
      },
      {
        title: "Private Bahasa",
        description: `	Pembelajaran bahasa yang dilaksanakan secara privat. Terdapat 25 pilihan bahasa sesuai dengan silabus 
        dan modul pembelajaran yang sudah disediakan. Satu kelas akan diisi maksimal 2 siswa untuk kelas eksklusif, 
        dan maksimal 5 siswa untuk privat grup. Pembelajaran akan diadakan dalam 10 pertemuan selama satu bulan.        `,
        image: PrivateClass
      },
      {
        title: "Mahir Bahasa (Kelas Musiman)",
        description: `Cakrawala Bahasa juga menyediakan pembelajaran bahasa musiman menjelang hari raya tertentu, 
        di antaranya Bahasa Mandarin saat Hari Raya Imlek, dan Bahasa Arab saat Bulan Ramadhan.`,
        image: MahirBahasa
      },
      {
        title: "CB for School",
        description: `Cakrawala Bahasa membuka kesempatan bagi sekolah-sekolah dari beragam tingkat untuk dapat bekerja sama 
        menyelenggarakan program pembelajaran bahasa yang dapat diintegrasikan dengan mata pelajaran di sekolah. 
        Program ini akan berjalan ketika kelas mencapai 30 siswa.`,
        image: ForSchool
      },
      {
        title: "Penerjemah",
        description: `Anda membutuhkan jasa penerjemah tersumpah untuk menerjemahkan dokumen? Cakrawala Bahasa siap menerjemahkan 
        berbagai dokumen dari Bahasa Inggris ke Bahasa Indonesia, dan sebaliknya.`,
        image: Penerjemah
      }
    ];
  }

  programTemplate() {
    const programs = this.state;
    return (
      <>
      {programs.map(i => {
        return <div className="row my-4">
          <h3 className={`${styleOurPrograms.h3Mobile} text-bold`}>{i.title}</h3>
          <div className="col-lg-4 col-md-6">
            <img
              src={i.image}
              className="img-fluid"
              alt={`Program ${i.title}`}
            />
          </div>
          <div className="col-lg-8 col-md-6">
            <h3 className={`${styleOurPrograms.h3Desktop} text-bold`}>{i.title}</h3>
            <p className={styleOurPrograms.p}>
              {i.description}
            </p>
          </div>
        </div>
      })}
      </>
    )
  };

  render() {
    return (
      <>
        <Header {...this.props}></Header>

        <Helmet>
          <title>Program Kami | Cakrawala Bahasa</title>
          <meta name="description" content="Belajar 25 bahasa dengan native speakers sekaligus beramal membantu pendidikan duafa? Tunggu apa lagi? Kembangkan kemampuan polyglot-mu bersama Cakrawala Bahasa!" />
          <meta name="keywords" content="cakrawala bahasa, kursus bahasa, kelas bahasa, kursus bahasa online, program cakrawala bahasa, kelas bahasa online, kursus bahasa asing, native speakers, platform bahasa, belajar bahasa, belajar bahasa daerah, belajar bahasa asing online" />
          <link rel="canonical" href="https://cakrawalabahasa.com/our-programs" />

          <meta property="og:title" content="Program Kami | Cakrawala Bahasa" />
          <meta property="og:description" content="Belajar 25 bahasa dengan native speakers sekaligus beramal membantu pendidikan duafa? Tunggu apa lagi? Kembangkan kemampuan polyglot-mu bersama Cakrawala Bahasa!" />
          <meta property="og:image" content={LogoBrand} />

          <meta name="twitter:title" content="Program Kami | Cakrawala Bahasa" />
          <meta name="twitter:description" content="Belajar 25 bahasa dengan native speakers sekaligus beramal membantu pendidikan duafa? Tunggu apa lagi? Kembangkan kemampuan polyglot-mu bersama Cakrawala Bahasa!" />
          <meta name="twitter:image" content={LogoBrand} />
        </Helmet>

        <div className="container our-programs">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li>
                <Link to="/">
                  <FontAwesomeIcon icon={faHome} />
                </Link>
              </li>
              <li>
                <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Programs
              </li>
            </ol>
          </nav>

          <h1 className="text-bold mb-5">Our Programs</h1>
          <h2 className="text-bold">Language Courses</h2>

          {this.programTemplate()}

        <div className="row">
          <a href="https://tokko.io/cakrawalabahasa" target="blank" type="button" class="btn btn-warning text-light">Kunjungi Marketplace</a>
        </div>

        </div>

        <Footer {...this.props}></Footer>
      </>
    );
  }
}
