import React, { Component } from "react";
import { countPageView } from 'adapters/landingPageAdapter'

import Header from "components/parts/Header";
import Footer from "components/parts/Footer";
import BannerLandingPageComponent from "components/landingPageComponent/BannerLandingPageComponent";
import OurProgramsLandingPageComponent from "components/landingPageComponent/OurProgramsLandingPageComponent";
import NewsComponent from "components/landingPageComponent/NewsLandingPageComponent";

import {Helmet} from "react-helmet";
import LogoBrand from "assets/images/logo/logoBrand.jpg";

export default class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artikels: [],
    };
  }

  async countPageView() {
    await countPageView('pageview')
      .then((response) => {
      })
      .catch((error) => {
      });
  }

  componentDidMount() {
    this.countPageView();
  }


  render() {
    return (
      <>
        <Header {...this.props}></Header>
        <div className="home__social">
        <div id="landing-page">

          <Helmet>
            <title>Cakrawala Bahasa | Kursus Bahasa dengan Native Speakers Online</title>
            <meta name="description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
            <meta name="keywords" content="cakrawala bahasa, kursus bahasa, kelas bahasa, kursus bahasa online, kelas bahasa online, kursus bahasa asing, native speakers, platform bahasa, belajar bahasa, belajar bahasa daerah, belajar bahasa asing online" />
            <link rel="canonical" href="https://cakrawalabahasa.com/" />

            <meta property="og:title" content="Cakrawala Bahasa | Kursus Bahasa dengan Native Speakers Online" />
            <meta property="og:description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
            <meta property="og:image" content={LogoBrand} />

            <meta name="twitter:title" content="Cakrawala Bahasa | Kursus Bahasa dengan Native Speakers Online" />
            <meta name="twitter:description" content="Komunitas dan platform yang menyediakan berbagai layanan belajar dan jasa bahasa (Bahasa Inggris, Jepang, Korea, Spanyol, Arab, …) untuk pemula dengan harga yang terjangkau. Mari bergabung bersama kami! | Cakrawala Bahasa" />
            <meta name="twitter:image" content={LogoBrand} />
          </Helmet>

          <BannerLandingPageComponent />
          
          </div>

          <OurProgramsLandingPageComponent />
          <NewsComponent artikels={this.state.artikels}/>

        </div>

        <Footer {...this.props}></Footer>

      </>
    );
  }
}
