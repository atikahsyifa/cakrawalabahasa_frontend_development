import React, { Component } from "react";
import { Link } from "react-router-dom";
import { faChevronRight, faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Slider from "react-slick";

import Header from "components/parts/Header";
import Footer from "components/parts/Footer";

import Hero from "assets/images/aboutUs/hero.webp";
import Social from "assets/images/aboutUs/social.png";
import Economy from "assets/images/aboutUs/economy.png";
import Study from "assets/images/aboutUs/study.png";

import Risqy from "assets/images/aboutUs/Rizqy_CEO.webp";
import Fachry from "assets/images/aboutUs/Fachri_CTO.webp";
import Ilham from "assets/images/aboutUs/Ilham_CPO.webp";
import Nura from "assets/images/aboutUs/Nura_operator.webp";
import Bibit from "assets/images/aboutUs/Bibit_CMO.webp";
import Windy from "assets/images/aboutUs/Windy_HR.webp";
import Yoga from "assets/images/aboutUs/Rafie_IT .webp";
import Zulfi from "assets/images/aboutUs/Zulfi_Media.webp";
import Farkhan from "assets/images/aboutUs/Farkhan_CFO.webp";
import Bismo from "assets/images/aboutUs/Bismo_Membership.webp";
import Dzaki from "assets/images/aboutUs/Dzaki_Program service.webp";

import styleAboutUs from "styles/aboutUs.module.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import {Helmet} from "react-helmet";
import LogoBrand from "assets/images/logo/logoBrand.jpg";

export default class AboutUs extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.state = {
      numberOfSlide: null
    };
    this.imageList = [Risqy, Fachry, Ilham, Bibit, Nura, Yoga, Zulfi, Windy, Dzaki, Farkhan, Bismo]
  }

  componentDidMount() {
    const numberOfSlide = this.numberOfSlidesToShow();
    this.setState({ numberOfSlide: numberOfSlide });
  }
  numberOfSlidesToShow() {
    if (window.matchMedia("(min-width: 992px)").matches) return 4;
    else return 2;
  }

  next() {
    this.slider.slickNext();
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: this.state.numberOfSlide,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2500
    };
    return (
      <>
        <Header {...this.props}></Header>

        <Helmet>
          <title>Tentang Kami | Cakrawala Bahasa</title>
          <meta name="description" content="Cakrawala Bahasa didirikan sejak tahun 2019 untuk memberikan pendidikan dan layanan bahasa berkualitas sekaligus mendukung tujuan pembangunan berkelanjutan atau SDG’s di bidang pertumbuhan ekonomi dan kualitas pendidikan." />
          <meta name="keywords" content="cakrawala bahasa, kursus bahasa, kelas bahasa, tentang kami, tentang cakrawala bahasa, kursus bahasa online, kelas bahasa online, kursus bahasa asing, native speakers, platform bahasa, belajar bahasa, belajar bahasa daerah, belajar bahasa asing online" />
          <link rel="canonical" href="https://cakrawalabahasa.com/about-us" />

          <meta property="og:title" content="Tentang Kami | Cakrawala Bahasa" />
          <meta property="og:description" content="Cakrawala Bahasa didirikan sejak tahun 2019 untuk memberikan pendidikan dan layanan bahasa berkualitas sekaligus mendukung tujuan pembangunan berkelanjutan atau SDG’s di bidang pertumbuhan ekonomi dan kualitas pendidikan." />
          <meta property="og:image" content={LogoBrand} />
          
          <meta name="twitter:title" content="Tentang Kami | Cakrawala Bahasa" />
          <meta name="twitter:description" content="Cakrawala Bahasa didirikan sejak tahun 2019 untuk memberikan pendidikan dan layanan bahasa berkualitas sekaligus mendukung tujuan pembangunan berkelanjutan atau SDG’s di bidang pertumbuhan ekonomi dan kualitas pendidikan." />
          <meta name="twitter:image" content={LogoBrand} />
        </Helmet>

        <div
          id="jumbotron"
          className="jumbotron d-flex align-items-center mb-5"
          style={{ backgroundImage: `url(${Hero})` }}
        ></div>

        <div className="container">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li>
                <Link to="/">
                  <FontAwesomeIcon icon={faHome} />
                </Link>
              </li>
              <li>
                <FontAwesomeIcon icon={faChevronRight} className="mx-3" />
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                About Us
              </li>
            </ol>
          </nav>

          <h2 className="mb-5">Profil</h2>
          <p>
            Cakrawala Bahasa didirikan sejak tahun 2019 oleh Mahasiswa
            Universitas Indonesia, seiring perkembangan Cakrawala Bahasa
            digiatkan oleh berbagai Mahasiswa Indonesia yang menempuh studi di
            dalam dan di luar negeri untuk memberikan pendidikan dan layanan
            bahasa berkualitas sekaligus mendukung tujuan pembangunan
            berkelanjutan atau SDG's di bidang pertumbuhan ekonomi dan kualitas
            pendidikan.
          </p>
          <p>
            Cakrawala Bahasa merupakan komunitas dan platform belajar yang
            bergerak di bidang pendidikan bahasa. Melalui program-program,
            Cakrawala Bahasa menyediakan berbagai layanan belajar dan jasa
            bahasa dengan harga yang terjangkau. Selain itu, Cakrawala Bahasa
            juga mewadahi relawan yang peduli terhadap pendidikan bahasa untuk
            memberikan layanan belajar bahasa bagi yatim dan dhuafa.
          </p>

          <h2 className="my-5">Latar Belakang</h2>
          <p>
            Cakrawala Bahasa berawal dari sebua mimpi besar founder kami yaitu
            "menjembatani seluruh guru bahasa dan murid di seluruh dunia untuk
            meruntuhkan setiap batasan dalam pebelajaran bahasa baik secara
            wilayah, budaya maupun kelas sosial". Impian tersebut berlandaskan
            kepada tiga faktor yaitu sosial, pendidikan, dan ekonomi
          </p>

          <div className="row mt-5">
            <div className="col-sm-4 text-center">
              <img
                src={Social}
                className={`${styleAboutUs.pilarImg} img-fluid mb-4`}
                alt="social factor"
              />
              <h4 className="text-bold">Sosial</h4>
              <p>
                Potensi PendudukUsia Muda <br />
                (BPS, 2021)
              </p>
            </div>
            <div className="col-sm-4 text-center">
              <img
                src={Economy}
                className={`${styleAboutUs.pilarImg} img-fluid mb-4`}
                alt="economy factor"
              />
              <h4 className="text-bold">Ekonomi</h4>
              <p>
                Kemampuan bahasa menentukan karir (Unniversity College Londodn,
                2011)
                <br />
                Tren bahasa meningkat (BPS, 2016)
                <br />
                Ekonomi menengah meningkat (World Bank, 2020)
              </p>
            </div>
            <div className="col-sm-4 text-center">
              <img src={Study}
              className={`${styleAboutUs.pilarImg} img-fluid mb-4`}
              alt="study factor" />
              <h4 className="text-bold">Pendidikan</h4>
              <p>
                Tingkat literasi Indonesia (UNESCO, 2016)
                <br />
                Kesenjangan pendidikan di Indonesia(PISA, 2019)
              </p>
            </div>
          </div>
        </div>

        <div className={`${styleAboutUs.history} my-5 py-5`}>
          <div className="container">
            <div className="row">
              <div className="col-md-3">
                <h2>Sejarah</h2>
              </div>
              <div className={`${styleAboutUs.desktopViewHistoy} col-md-9`}>
                <div className="row">
                  <div className="col-md-4">
                    <div className={`${styleAboutUs.circleOrange} my-4`}>
                      <div className={styleAboutUs.line}></div>
                      <div className={styleAboutUs.circleOrange}></div>
                    </div>
                    <p>
                      Start-up penddikan bahasa yang didirikan oleh mahasiswa
                      FIB UI dan menyediakan berbagai layanan kebutuhan bahasa
                      dimulai dari Privat Bahasa
                    </p>
                  </div>
                  <div className="col-md-4">
                    <div className={`${styleAboutUs.circleOrange} my-4`}></div>
                    <p>
                      Start-up pendidikan bahasa berbasis komunitas yang
                      digiatkan oleh mahasiswa Indonesia di dalam dan di luar
                      negeri, menyediakan berbagai layanan kebutuhan bahasa dan
                      juga beasiswa pendidikan bagi kalangan underprivileged
                    </p>
                  </div>
                  <div className="col-md-4">
                    <div className={`${styleAboutUs.circleOrange} my-4`}></div>
                    <p>
                      Start-up pendidikan bahasa berkualitas dengan tutor
                      kompeten disertai native speaker dari berbagai negara
                      serta menyediakan berbagai layanan kebutuhan bahasa
                      sekaligus memberikan beasiswa pendidikan bagi kalangan
                      underprivileged
                    </p>
                  </div>
                </div>
              </div>
              <div className={styleAboutUs.mobileViewHistoy}>
                <div className="row">
                  <div className="col-6 d-flex align-items-center">
                    <div className={`${styleAboutUs.circleOrange} my-4`}>
                      <div className={styleAboutUs.line}></div>
                      <div className={styleAboutUs.circleOrange}></div>
                    </div>
                  </div>
                  <div className="col-6">
                    <p>
                      Start-up penddikan bahasa yang didirikan oleh mahasiswa
                      FIB UI dan menyediakan berbagai layanan kebutuhan bahasa
                      dimulai dari Privat Bahasa.
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-6 d-flex align-items-center">
                    <div className={`${styleAboutUs.circleOrange} my-4`}></div>
                  </div>
                  <div className="col-6">
                    <p>
                      Start-up pendidikan bahasa berbasis komunitas yang
                      digiatkan oleh mahasiswa Indonesia di dalam dan di luar
                      negeri, menyediakan berbagai layanan kebutuhan bahasa dan
                      juga beasiswa pendidikan bagi kalangan underprivileged.
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-6 d-flex align-items-center">
                    <div className={`${styleAboutUs.circleOrange} my-4`}></div>
                  </div>
                  <div className="col-6">
                    <p>
                      Start-up pendidikan bahasa berkualitas dengan tutor
                      kompeten disertai native speaker dari berbagai negara
                      serta menyediakan berbagai layanan kebutuhan bahasa
                      sekaligus memberikan beasiswa pendidikan bagi kalangan
                      underprivileged.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container my-5 py-5">
          <div>
            <h2 className="mb-4">Our Team</h2>
            <div className="row">
              <div className={`${styleAboutUs.sliderWrap} col-11`}>
                <Slider ref={(c) => (this.slider = c)} {...settings}>
                  {this.imageList.map((image) => (
                  <div className={`${styleAboutUs.cardSlider} px-1`}>
                    <div className="card">
                      <div className="row">
                        <img
                          className="img-fluid"
                          src={image}
                          alt="Card cap"
                        />
                      </div>
                    </div>
                  </div>
                  ))}
                </Slider>
              </div>
              <div className="col-1 d-none d-lg-block">
                <div className={`${styleAboutUs.btnSliderNext} btn d-flex`} onClick={this.next}>
                  <FontAwesomeIcon
                    className="d-flex align-self-center"
                    size="5x"
                    icon={faChevronRight}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer {...this.props}></Footer>
      </>
    );
  }
}
