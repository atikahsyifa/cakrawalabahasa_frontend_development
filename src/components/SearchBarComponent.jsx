import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { searchArtikel } from 'adapters/newsAdapter';
import Select from 'react-select';

export default function SearchBarComponent(props) {
    const [artikels, setArtikel] = useState([]);
    const [selectedOption, setSelectedOption] = useState(null);

    useEffect(() => {
        const getArticle = async (perPage, currentPage, kategoriId, searchText) => {
            try {
                const payload = {
                    per_page: perPage,
                    current_page: currentPage,
                    kategori: kategoriId,
                    searchText: searchText
                };
            await searchArtikel("artikel/artikel-list", payload)
                .then((resp) => {
                    let datas = resp.data.artikels;
                    datas.map((data, index) => {
                        datas[index].linkJudul = data.judul.replace(/ /g, "-");
                        datas[index].dateUpload = data.created_at.split("T")[0];
                        datas[index].timeUpload = data.created_at
                        .split("T")[1]
                        .split(".000000Z")[0];
                    });
                    setArtikel(datas);
                })
                .catch((error) => {
                    window.$toast.fire({
                        icon: "error",
                        title: error.response.data.message || "Connection Error",
                    });
                });
            } catch (err) {
            window.$toast.fire({
                icon: "error",
                title: "Connection Error",
            });
            };
        }
        getArticle();
      }, []);

    const handleChange = selectedOption => {
        
    }

    return (
        <div>
            {artikels.map((artikel) => (
                <Select
                value={selectedOption}
                options={artikel.judul}
                onChange={handleChange}
                placeholder="Search"
                openMenuOnClick={false}
                classNamePrefix= "select" />
            ))}
        </div>
    )
}