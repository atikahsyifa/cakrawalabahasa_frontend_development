import React, { useState } from "react";
import { Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Redirect } from 'react-router';
import Axios from "axios";

import { saveToken } from "store/storageToken";
import { saveData } from "store/storageData";

export default function SignInFormComponent(props) {
    const [busy, setBusy] = useState("d-none");
    const [form, setForm] = useState({});
    const [errorForm, setErrorForm] = useState({
        akun: "",
        password: "",
    });
    const [validated, setValidated] = useState("");
    const [redirect, setRedirect] = useState(false);


    const handleInputChange = event => {
        const target = event.target;
        const value = target.value;
        const formTemp = { ...form };
        formTemp[target.name] = value;
        setForm({ ...formTemp });
    }
    
    const handleSubmit = event => {
        submitForm();
        event.preventDefault();
    }

    const submitForm = async e => {
        try {
          setErrorForm({});
          setValidated("");
          setBusy("d-block");
          const payload = form;
          await window.$axios
            .post(`${window.$urlApi}sign-in`, payload)
            .then((response) => {
              const tokenType = response.data.token_type
              const token = response.data.access_token
              const userData = response.data.user
              Axios.defaults.headers.common.Authorization = tokenType + ' ' + token
              saveToken(token, tokenType)
              saveData('user', userData)
              
              window.$toast.fire({
                icon: "success",
                title: response.data.message,
              });
    
              setRedirect(true)
            })
            .catch((error) => {
              window.$toast.fire({
                icon: "error",
                title: error.response.data.message || "Connection Error",
              });
              setErrorForm(error.response.data.errors || error.response.data.message);
              setValidated(false);
            });
        } catch (err) {
          window.$toast.fire({
            icon: "error",
            title: "Connection Error",
          });
        };
        setBusy("d-none");
    }
    
    if (redirect) {
        return <Redirect to="/" />;
      }
  
      let errorAkun;
      let errorPassword;
      if (errorForm && errorForm.akun) {
        errorAkun = (
          <Form.Control.Feedback type="invalid">
            {errorForm.akun}
          </Form.Control.Feedback>
        );
      }
      if (errorForm && errorForm.password) {
        errorPassword = (
          <Form.Control.Feedback type="invalid">
            {errorForm.password}
          </Form.Control.Feedback>
        );
    }

    return (
        <div className="col-md-6 offset-md-1 p-5">
            <h1 className="text-bold">Sign In</h1>
            <Form validated={validated} onSubmit={handleSubmit} className="my-3">
            <Form.Group className="mb-5">
                <Form.Label>Email/Username</Form.Label>
                <Form.Control
                required
                name="akun"
                type="text"
                placeholder="Enter Text Here"
                value={form.akun}
                onChange={handleInputChange}
                />
                {errorAkun}
            </Form.Group>
            <Form.Group className="mb-5">
                <Form.Label>Password</Form.Label>
                <Form.Control
                required
                name="password"
                type="password"
                placeholder="Enter Text Here"
                value={form.password}
                onChange={handleInputChange}
                />
                {errorPassword}
            </Form.Group>
            <Button
                variant="warning"
                type="submit"
                className="text-light mt-5 m-auto d-flex text-center w-100"
            >
                <span
                className={`spinner-border text-light ${busy}`}
                role="status"
                aria-hidden="true"
                style={{ marginRight: "20px" }}
                ></span>
                <span className="m-auto">Sign In</span>
            </Button>
            </Form>
            <small>Belum punya akun ? <Link to="/sign-up"> Sign up </Link></small>
            <br />
            <small><Link to="/forgot-password"> Lupa Password ? </Link></small>
            <br />
            <small>Belum terima email verifikasi ? <Link to="/verifikasi-request"> Kirim ulang email verifikasi </Link></small>
        </div>
    )
}