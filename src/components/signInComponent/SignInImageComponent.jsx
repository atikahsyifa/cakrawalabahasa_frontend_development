import React from "react";
import SignInImage from "assets/images/sign/signIn.webp";

export default function SignInImageComponent(props) {
    return (
        <div className="col-md-4 d-none d-md-block">
            <img src={SignInImage} className="img-fluid " alt="signIn" />
        </div>
    )
}