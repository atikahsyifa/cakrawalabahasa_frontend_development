import React, { useState } from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import {
  ListGroup,
} from "react-bootstrap";

import searchArtikelNavbar from 'adapters/header'
import { Link } from "react-router-dom";

export default function SearchArtikelList() {
  const [keyword, setKeyword] = useState("");
  const [artikelList, setArtikelList] = useState([]);

  const submitForm = async () => {
    try {
      const payload = {
        searchText: keyword,
        per_page: 5,
        current_page: 1
      };
      await searchArtikelNavbar('artikel/artikel-list', payload)
        .then((response) => {
          let datas = response.data.artikels;
          datas.map((data, index) => {
              datas[index].linkJudul = data.judul.replace(/ /g, "-");
          });
          setArtikelList(datas)
        })
        .catch((error) => {
          const statusCode = error.response.status
          const message = error.response.data.errors
          switch (statusCode) {
            case 401:
              break
            default:
              window.$toast.fire({
                icon: "error",
                title: message || "Connection Error",
              });
          }
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }

  const handleInputChange = async (event) => {
    const value = event.target.value
    setKeyword(value)
    submitForm();
  }

  const resetList = () => {
    setKeyword("")
    console.log('KLIK')
  }

  return (
    <>
      <div className="form-group has-search search-insert" style={{position: "relative"}}>
        <span className="fa form-control-feedback"> <FontAwesomeIcon icon={faSearch} /> </span>
        <input type="text" className="form-control" placeholder="Search" onChange={(event) => handleInputChange(event)} />
        {
        keyword !== "" && 
        <ListGroup defaultActiveKey="#link1" style={{position: "absolute", width: "100%"}}>
          {
          artikelList.map((artikel) => (
            <Link
                to={`/news/detail/${artikel.id}/${artikel.linkJudul}`}
                style={{ textDecoration: "none" }}
                onClick={() => resetList()}
                key={artikel.id}
            >
              <ListGroup.Item action >
                { artikel.judul }
              </ListGroup.Item>
            </Link>
          ))
          }
        </ListGroup>
        }
      </div>
    </>
  )
}
