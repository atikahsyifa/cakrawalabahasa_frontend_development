import React from "react";
import { Link } from "react-router-dom";

import "styles/header.css";
// import { faGlobe } from "@fortawesome/free-solid-svg-icons";

import {
  Navbar,
  Dropdown,
  NavDropdown,
  Nav,
  Button,
} from "react-bootstrap";
import { checkToken } from "store/storageToken";
import getData from 'store/storageData'

import CTA from "assets/images/landingPage/cta.webp";
import WAlogo from "assets/images/landingPage/WAlogo.webp";

import LogoBrand from "assets/images/logo/logoBrand.png";
import BlankProfile from "assets/images/profile/blankProfile.png";
import SearchArtikelList from "components/parts/SearchArtikelList";
// import flagEnglish from "assets/images/icon/flag-english.png";
// import flagIndonesia from "assets/images/icon/flag-indonesia.png";

export default function Header(props) {
  return (
    <>
      <div className="container">
        <Navbar expand="md">
          <Navbar>
            <Link to="/">
              <img
                src={LogoBrand}
                className="d-inline-block align-top logo-brand"
                alt="Cakrawala Bahasa Logo"
              />
            </Link>
          </Navbar>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse
            className="justify-content-end"
            id="basic-navbar-nav"
          >
            <Nav className="mr-auto">
              <NavDropdown title="Menu " id="basic-nav-dropdown">
                <Link to="/our-programs"> Our Programs </Link>
                <Link to="/news"> News </Link>
                {/* <Link to="/registration"> Registration </Link> */}
              </NavDropdown>
              <Link to="/about-us"> About </Link>
              <Link to="/contact"> Contact </Link>
              <Link to="/career"> Career </Link>
            </Nav>

            <SearchArtikelList />

            {/* <FontAwesomeIcon
              className="d-none d-lg-block d-sm-none"
              icon={faGlobe}
            /> */}
            {/* <NavDropdown title="Eng" id="basic-nav-dropdown">
                <NavDropdown.Item href="/">
                  <img
                    src={flagEnglish}
                    className="d-inline-block align-top icon"
                    alt="Youtube Icon"
                  />
                  English
                </NavDropdown.Item>
                <NavDropdown.Item href="/">
                  <img
                    src={flagIndonesia}
                    className="d-inline-block align-top icon"
                    alt="Youtube Icon"
                  />
                  Indonesia
                </NavDropdown.Item>
              </NavDropdown> */}
            {
              checkToken() !== undefined && checkToken() !== null
                ?
                <Dropdown>
                  <Link to="/profile">
                    <img
                      src={`${window.$urlPublicUser}/img/profile/${getData('user')["photo_profile"]}`}
                      onError={(ev) => ev.target.src = BlankProfile}
                      className="d-inline-block align-top photo-profile"
                      alt="profile"
                    />
                  </Link>

                  <Dropdown.Toggle split size="sm" variant="light" id="dropdown-split-basic" style={{ backgroundColor: 'transparent', borderColor: 'transparent' }} />

                  <Dropdown.Menu>
                    <Dropdown.Item>
                      <Link to="/profile"> Profile </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link to="/logout"> Logout </Link>
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
                :
                <>
                  <Nav className="mr-auto">
                    <Link to="/sign-in">Sign In</Link>
                  </Nav>
                  <Nav className="mr-auto">
                    <Link to="/sign-up">
                      <Button variant="warning" className="text-light">
                        Sign Up
                      </Button>
                    </Link>
                  </Nav>
                </>
            }
          </Navbar.Collapse>
        </Navbar>
      </div>


      <div className="d-none d-md-block">
        <a className="text-decoration-none" target="blank" href="https://api.whatsapp.com/send?phone=+6285894885963&text=Hi%20kak%20aku%20mau%20tanya-tanya%20seputar%20cakrawalabahasa%20dong">
          <img
            src={CTA}
            alt="cta-button"
            style={{
              position: 'fixed',
              bottom: 0,
              right: 0,
              zIndex: 99,
              width: '200px'
            }}
          />
        </a>
      </div>
      <div className="d-block d-sm-block d-md-none">
        <a className="text-decoration-none" target="blank" href="https://api.whatsapp.com/send?phone=+6285894885963&text=Hi%20kak%20aku%20mau%20tanya-tanya%20seputar%20cakrawalabahasa%20dong">
          <img
            src={WAlogo}
            alt="cta-button"
            style={{
              position: 'fixed',
              bottom: '10px',
              right: '10px',
              zIndex: 99,
              width: '75px'
            }}
          />
        </a>
      </div>
    </>
  );
}
