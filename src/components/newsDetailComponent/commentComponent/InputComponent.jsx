import React, { useState } from 'react'

import { 
  Form,
  Button,
  Card
 } from "react-bootstrap";

export default function InputComponent(props) {
  const [comment, setComment] = useState("");

  const handleInputChange = event => {
    const value = event.target.value
    setComment(value);
    props.changeInput(value);
  }

  const handleSubmit = async (event) => {
    await props.submit(event);
    await setComment('')
  }
  
  return (
    <>
      <Card className="my-5">
        <Card.Body>
          { props.topSlot }
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="commentTextarea">
              <Form.Label>{ props.title }</Form.Label>
              <Form.Control as="textarea"
              rows={3}
              required
              name="comment"
              placeholder="Enter Comment Here"
              minLength="10"
              value={comment}
              onChange={(event) => handleInputChange(event)}/>
            </Form.Group>
            <div className="row text-right">
              <div className="col-md-4 offset-md-8">
                <Button
                className="text-light col-12 d-flex"
                variant="warning"
                type="submit"
                value="submit"
                >
                  <span
                    className={`spinner-border text-light ${props.busy}`}
                    role="status"
                    aria-hidden="true"
                  ></span>
                  <span className="m-auto">Send</span>
                </Button>
              </div>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </>
  )
}
