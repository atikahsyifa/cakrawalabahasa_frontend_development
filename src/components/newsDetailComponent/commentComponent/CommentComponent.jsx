import React, { useState, useEffect  } from 'react'

import {getArtikelsComment} from 'adapters/newsAdapter'

import InputComponent from "components/newsDetailComponent/commentComponent/InputComponent";
import PreviosCommentComponent from "components/newsDetailComponent/commentComponent/PreviosCommentComponent";
import {sendArtikelsComment} from 'adapters/newsAdapter'
import { checkToken } from "store/storageToken";

export default function CommentComponent(props) {
  const [comments, setComments] = useState([]);
  const id = props.idArtikel;

  const [busy, setBusy] = useState("d-none");
  const [form, setForm] = useState({});

  const handleBalasCommentToggle = e => {
    const index = comments.findIndex(comment => comment.id === e) // find index
    let tempComments = [ ...comments ]
    tempComments[index].showBalasComment = tempComments[index].showBalasComment ? false : true
    setComments(tempComments)
  }

  const handleUpdateComment = e => {
    const index = comments.findIndex(comment => comment.id === e.id) // find index
    let tempComments =[ ...comments ]
    tempComments[index] = e
    setComments(tempComments)
  }

  const submitForm = async e => {
    try {
      setBusy( "d-block" );
      const payload = {
        comment: form.comment,
        id: id
      };
      await sendArtikelsComment('artikel/artikels-comment', payload)
        .then((response) => {
          let oldComments = [ ...comments ];
          oldComments.unshift(response.data.comment)
          setComments(oldComments);

          window.$toast.fire({
            icon: "success",
            title: response.data.message,
          });
        })
        .catch((error) => {
          const statusCode = error.response.status
          const message = error.response.data.errors.comment
          switch (statusCode) {
            case 401:
              window.$toast.fire({
                icon: "error",
                title: "Sebelum post komentar, kamu harus login dulu",
              });
              break
            default:
              window.$toast.fire({
                icon: "error",
                title: message || "Connection Error",
              });
          }
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    setBusy( "d-none" );
    setForm({});
  }

  const handleInputChange = value => {
    const formTemp = { ...form };
    formTemp['comment'] = value;
    setForm({ ...formTemp });
  }
  
  const handleSubmit = event => {
    submitForm();
    event.preventDefault();
  }
  
  useEffect(() => {
    const getComments = async e => {
      try {
        const url = (checkToken() !== undefined && checkToken() !== null) ? `artikel/artikels-comment/${id}/with-artikel-id/login` : `artikel/artikels-comment/${id}/with-artikel-id`
        await getArtikelsComment(url)
          .then((response) => {
            setComments(response.data.comments);
          })
          .catch((error) => {
            window.$toast.fire({
              icon: "error",
              title: error.response.data.message || "Connection Error",
            });
          });
      } catch (err) {
        window.$toast.fire({
          icon: "error",
          title: "Connection Error",
        });
      };
    }
    getComments();
  }, [id]);

  return (
    <div>
      <InputComponent changeInput={(value) => handleInputChange(value)} submit={(event) => handleSubmit(event)} busy={busy} title='Post Comment' />
      <PreviosCommentComponent comments={comments} updateComment={(event) => handleUpdateComment(event)} balasComment={(event) => handleBalasCommentToggle(event)} />
    </div>
  )
}
