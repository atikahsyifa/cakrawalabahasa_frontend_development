import React, { useState } from 'react'

import { faThumbsUp, faComment, faFlag } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Card } from "react-bootstrap";
import { likeComment, sendBalasComment } from 'adapters/newsAdapter'
import InputComponent from "components/newsDetailComponent/commentComponent/InputComponent";

import styleComment from "styles/news/comment.module.css";
import PopUpReport from 'components/newsDetailComponent/PopUpReport';

export default function PreviosCommentComponent(props) {
  const comments = props.comments;

  const [busy, setBusy] = useState("d-none");
  const [form, setForm] = useState({});
  const [commentToReport, setCommentToReport] = useState({});
  const [modalShow, setModalShow] = useState(false);

  const handleLike = async (id, action) => {
    try {
      const url = action === 'like' ? 'artikel/like-comment' : 'artikel/like-comment-balas'
      const payload = {
        id: id
      };
      await likeComment(url, payload)
        .then((response) => {
          props.updateComment(response.data.comment);
          window.$toast.fire({
            icon: "success",
            title: response.data.message,
          });
        })
        .catch((error) => {
          const statusCode = error.response.status
          const message = error.response.data.errors
          switch (statusCode) {
            case 401:
              window.$toast.fire({
                icon: "error",
                title: "Sebelum like komentar, kamu harus login dulu",
              });
              break
            default:
              window.$toast.fire({
                icon: "error",
                title: message || "Connection Error",
              });
          }
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
  }
  
  const submitForm = async id => {
    try {
      setBusy( "d-block" );
      const payload = {
        comment: form.comment,
        id: id
      };
      await sendBalasComment('artikel/balas-comment', payload)
        .then((response) => {
          props.updateComment(response.data.comment);
          window.$toast.fire({
            icon: "success",
            title: response.data.message,
          });
        })
        .catch((error) => {
          const statusCode = error.response.status
          const message = error.response.data.errors.comment
          switch (statusCode) {
            case 401:
              window.$toast.fire({
                icon: "error",
                title: "Sebelum post komentar, kamu harus login dulu",
              });
              break
            default:
              window.$toast.fire({
                icon: "error",
                title: message || "Connection Error",
              });
          }
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    setBusy( "d-none" );
    setForm({});
  }

  const handleInputChange = value => {
    const formTemp = { ...form };
    formTemp['comment'] = value;
    setForm({ ...formTemp });
  }
  
  const handleSubmit = (event, id) => {
    submitForm(id);
    event.preventDefault();
  }

  const showBalasInput = e => {
    props.balasComment(e);
  }

  const setCommentChosen = e => {
    setCommentToReport(e)
  }

  return (
    <>
      <div className="row">
        {comments.map(comment => (

        <Card key={comment.id}>

          <Card.Body>
            <div className="media">
              <div className="row d-flex justify-content-start">
                <div className="col-2 col-lg-1">
                  <img src={`${window.$urlPublicUser}/img/profile/${comment.user.photo_profile}`}
                  className={`${styleComment.commentImage} media-object rounded-circle`} />
                </div>
                <div className="col col-md-10 my-auto">
                  <h4 className={`${styleComment.commentTitle} media-heading`}>{comment.user.username}</h4>
                  <small>{comment.created_at.split("T")[0]}</small>
                </div>
              </div>
              <div className="media-body mt-2">
                <p className={`${styleComment.title} komen`}>
                    {comment.comment}<br />
                </p>

                <div className="row">
                  <p className="d-inline-block w-auto">
                    {comment.like_comment !== undefined && comment.like_comment.length > 0
                    ?
                    <FontAwesomeIcon icon={faThumbsUp}
                    className={`${styleComment.likeButton} text-primary`}
                    onClick={() => handleLike(comment.id, 'like')}
                    title="Like komentar" />
                    :
                    <FontAwesomeIcon icon={faThumbsUp}
                    className={styleComment.likeButton}
                    onClick={() => handleLike(comment.id, 'like')}
                    title="Like komentar" />
                    }
                    {comment.like}
                  </p>

                  <p className="d-inline-block w-auto">
                    <FontAwesomeIcon icon={faComment}
                    className={styleComment.commentButton}
                    onClick={() => showBalasInput(comment.id)}
                    title="Balas komentar"
                    />
                  </p>

                  <p className="d-inline-block w-auto">
                    <FontAwesomeIcon icon={faFlag}
                    className={styleComment.reportButton}
                    onClick={() => (setModalShow(true), setCommentChosen({type: 'comment', id: comment.id}))}
                    title="Laporkan komentar"
                    />
                  </p>

                </div>
              </div>
            </div>
      
            {comment.balas.map(balas => (
            <div className={styleComment.balas} key={balas.id}>
              <div className="media">
                <div className="row d-flex justify-content-start mb-1">
                  <div className="col-2 col-lg-1 my-auto">
                    <img src={`${window.$urlPublicUser}/img/profile/${balas.user.photo_profile}`}
                    className={`${styleComment.balasImage} media-object rounded-circle`} />
                  </div>
                  <div className="col col-md-10 my-auto">
                    <h4 className={`${styleComment.commentTitle} media-heading`}>{balas.user.username}</h4>
                    <small>{balas.created_at.split("T")[0]}</small>
                  </div>
                </div>

                <div className="media-body mt-2">
                  <p className={`${styleComment.title} komen`}>
                    {balas.comment}<br />
                  </p>

                  <div className="row">
                    <p className="d-inline-block w-auto">
                      {balas.like_comment_balas !== undefined && balas.like_comment_balas.length > 0
                      ?
                      <FontAwesomeIcon icon={faThumbsUp}
                      className={`${styleComment.likeButton} text-primary`}
                      onClick={() => handleLike(balas.id, 'like balas')}
                      title="Like komentar" />
                      :
                      <FontAwesomeIcon icon={faThumbsUp}
                      className={styleComment.likeButton}
                      onClick={() => handleLike(balas.id, 'like balas')}
                      title="Like komentar" />
                      }
                      {balas.like}
                    </p>

                    <p className="d-inline-block w-auto">
                    <FontAwesomeIcon icon={faFlag}
                    className={styleComment.reportButton}
                    onClick={() => (setModalShow(true), setCommentChosen({type: 'balas', id: balas.id}))}
                    title="Laporkan komentar"
                    />
                    </p>

                  </div>
                </div>
              </div>
            </div>
            ))}
            {comment.showBalasComment && 
              <div className="row">
                <InputComponent changeInput={(value) => handleInputChange(value)} submit={(event) => handleSubmit(event, comment.id)} busy={busy} title="Post Balas Comment" />
              </div>
              }
            </Card.Body>
        </Card>
        ))}
      </div>

      <PopUpReport modalShow={modalShow} commentChosen={commentToReport} modalHide={() => setModalShow(false)} />
    </>
  )
}
