import React, { useState } from 'react'
import { Form, Modal, Button } from "react-bootstrap";
import { reportComment } from 'adapters/newsAdapter'
// import "styles/popUpReport.css"

export default function PopUpReport(props) {
  const modalShow = props.modalShow;
  const commentChosen = props.commentChosen;
  const [form, setForm] = useState({});
  const [busy, setBusy] = useState("d-none");

  const submitForm = async () => {
    try {
      setBusy( "d-block" );
      const url = commentChosen.type === 'comment' ? 'report/report-comment' : 'report/report-balas-comment'
      const payload = form;
      await reportComment(url, payload)
        .then((response) => {
          window.$toast.fire({
            icon: "success",
            title: response.data.message,
          });
          props.modalHide();
        })
        .catch((error) => {
          const statusCode = error.response.status
          const message = error.response.data.errors
          switch (statusCode) {
            case 401:
              window.$toast.fire({
                icon: "error",
                title: "Sebelum melaporkan komentar, kamu harus login dulu",
              });
              break
            default:
              window.$toast.fire({
                icon: "error",
                title: message || "Connection Error",
              });
          }
        });
    } catch (err) {
      window.$toast.fire({
        icon: "error",
        title: "Connection Error",
      });
    };
    setBusy( "d-none" );
  }

  const handleSubmit = event => {
    submitForm();
  }

  const handleInputChange = event => {
    const value = event.target.value
    const formTemp = { ...form };
    formTemp['message'] = value;
    formTemp['comment_id'] = commentChosen.id;
    setForm({ ...formTemp });
  }

  const handleModalHide = e => {
    props.modalHide();
  }

  return (
    <>
      <Modal
        show={modalShow}
        onHide={() => handleModalHide()}
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header>
          <Modal.Title id="example-modal-sizes-title-sm">
            Report Comment
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form className="mb-3">
            <Form.Label><h5>Laporkan komentar</h5></Form.Label>
            <Form.Check type="radio" name="report" id="via-spam" value="spam" label="Iklan dan spam" onChange={(event) => handleInputChange(event)} />
            <Form.Check type="radio" name="report" id="via-asusila" value="asusila" label="Porno atau asusila" onChange={(event) => handleInputChange(event)} />
            <Form.Check type="radio" name="report" id="via-sara" value="sara" label="Ujaran kebencian atau SARA" onChange={(event) => handleInputChange(event)} />
            <Form.Check type="radio" name="report" id="via-perundungan" value="perundungan" label="Perundungan atau pelecehan" onChange={(event) => handleInputChange(event)} />
            <Form.Check type="radio" name="report" id="via-lainnya" value="lainnya" label="Lainnya" onChange={(event) => handleInputChange(event)} />
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => handleModalHide()}>
            Cancel
          </Button>
          <Button
            variant="warning"
            className="text-light d-flex"
            onClick={() => handleSubmit()}
          >
            <span
              className={`spinner-border spinner-border-sm text-light ${busy}`}
              role="status"
              aria-hidden="true"
            ></span>
            <span className="m-auto">Send Report</span>
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}