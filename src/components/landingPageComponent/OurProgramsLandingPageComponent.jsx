import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";

import Program1 from "assets/images/landingPage/our_program1.webp";
import Program2 from "assets/images/landingPage/our_program2.webp";
import Program3 from "assets/images/landingPage/our_program3.webp";
import styleLanding from "styles/landingPage.module.css";

export default function OurProgramsLandingPageComponent(props) {
    return (
        <div className={styleLanding.ourProgram}>
            <div className="container">
            <h2 className="mb-4 text-bold" color="black">
                <Link to="/our-programs" className="text-decoration-none text-body">Our Programs</Link></h2>
            <div className={`${styleLanding.programList} row`}>

                <div className="col-12 col-md-8 mb-md-0 mb-4">
                <Link to="/our-programs">
                    <div className={`${styleLanding.imageProgram} p-2`}>
                    <img src={Program1} className={styleLanding.imgProgram} alt="Program 1" />
                    <div className={styleLanding.overlay}></div>
                    <div className={`${styleLanding.caption} text-light`}>
                        <h3>Privat Bahasa</h3>
                        <p>
                        Belajar bahasa secara privat secara online atau offline
                        dengan berbagai pilihan paket dan sesi yang memudahkan
                        pembelajaran.
                        </p>
                    </div>
                    </div>
                </Link>
                </div>

                <div className="col-12 col-md-4">

                <div className={`${styleLanding.secondImage} row pb-2`}>
                    <div className="col-12">
                    <Link to="/our-programs">
                        <div className={`${styleLanding.imageProgram} p-2`}>
                        <img
                            src={Program2}
                            className={`${styleLanding.imgProgram} img-fluid`}
                            alt="Program 2"
                        />
                        <div className={styleLanding.overlay}></div>
                        <div className={`${styleLanding.caption} text-light`}>
                            <h3>Kelas Reguler</h3>
                            <p>
                            Program kelas bahasa secara online selama 8 kali
                            pertemuan dalam satu bulan.
                            </p>
                        </div>
                        </div>
                    </Link>
                    </div>
                </div>

                <div className={`${styleLanding.thirdImage} row pt-2`}>
                    <div className="col-12">
                    <Link to="/our-programs">
                        <div className={`${styleLanding.imageProgram} p-2`}>
                        <img
                            src={Program3}
                            className={`${styleLanding.imgProgram} img-fluid`}
                            alt="Program 3"
                        />
                        <div className={styleLanding.overlay}></div>
                        <div className={`${styleLanding.caption} text-light`}>
                            <h3>Membership Program</h3>
                            <p>
                            Kami menyediakan berbagai kegiatan menarik melalui
                            Membership Program Cakrawala Bahasa.
                            </p>
                        </div>
                        </div>
                    </Link>
                    </div>
                </div>

                </div>
            </div>

            <div className="row mt-2 mt-md-4">
                <div className={`${styleLanding.linkMore} col-12`}>
                <Link to="/our-programs">
                    Show More <FontAwesomeIcon icon={faChevronRight} />
                </Link>
                </div>
            </div>
            
            </div>
        </div>
    )
}