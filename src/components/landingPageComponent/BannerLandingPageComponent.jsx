import Hero from "assets/images/landingPage/hero.webp";
import styleLanding from "styles/landingPage.module.css";

export default function BannerLandingPageComponent(props) {
    return (
        <div
            className={`${styleLanding.jumbotron} jumbotron d-flex align-items-center`}
            style={{ backgroundImage: `url(${Hero})` }}
          >
            <div className="container text-light">
              <div className="col-lg-8">
                <h1 className="display-5 mt-5 text-bold">Mari Bergabung Bersama Kami!</h1>
              </div>
              <div className="col-lg-5 mt-5">
                <p>
                  Selamat datang di situs resmi Cakrawala Bahasa! Temukan layanan
                  bahasa sesuai kebutuhanmu.
                </p>
              </div>
              <a
                className="btn btn-warning text-light mt-3"
                href="/our-programs"
                role="button"
              >
                Go to Our Programs
              </a>
            </div>
        </div>
    )
}