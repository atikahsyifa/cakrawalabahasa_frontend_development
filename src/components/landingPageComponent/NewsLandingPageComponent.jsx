import React, { useState, useEffect } from 'react';
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import styleLanding from "styles/landingPage.module.css";
import getArtikel from 'adapters/landingPageAdapter'

export default function NewsComponent(props) {
    const [artikels, setArtikel] = useState([]);

    useEffect(() => {
        const getArticle = async e => {
            try {
                await getArtikel('artikel/artikel')
                    .then((resp) => {
                    let datas = resp.data.artikels;
                    datas.map((data, index) => {
                        datas[index].linkJudul = data.judul.replace(/ /g, "-");
                    });
                    setArtikel(datas);
                    })
                    .catch((error) => {
                    window.$toast.fire({
                        icon: "error",
                        title: error.response.data.message || "Connection Error",
                    });
                    });
            } catch (err) {
                window.$toast.fire({
                icon: "error",
                title: "Connection Error",
                });
            };
        }
        getArticle();
      }, []);

    return (
        <div className={`${styleLanding.news} pt-4`}>
            <div className="container">
                <h2 className="my-4 text-light text-bold">
                <Link to="/news" className="text-decoration-none text-white">News</Link></h2>

                <div className="row">
                    {artikels.filter((artikel, index) => index < 3).map((artikel) => (
                        <div className="col-md-4 mb-3" key={artikel.id}>
                            <Card style={{ backgroundColor: "#1D2951" }} text="white">

                            <Link
                                to={`/news/detail/${artikel.id}/${artikel.linkJudul}`}
                                style={{ textDecoration: "none" }}
                                className="text-light"
                            >
                            <Card.Img
                                variant="top"
                                src={`${window.$urlPublic}/img/artikel/${artikel.image}`}
                            />
                            </Link>

                            <Card.Body>
                                <Link
                                to={`/news/detail/${artikel.id}/${artikel.linkJudul}`}
                                style={{ textDecoration: "none" }}
                                className="text-light"
                                >
                                <h5>{artikel.judul}</h5>
                                </Link>
                                <Card.Text>{artikel.text_lead}</Card.Text>
                            </Card.Body>

                            </Card>
                        </div>
                    ))}
                </div>

                <div className="row mt-2 mt-md-4">
                    <div className={`${styleLanding.linkMore} col-12`}>
                    <Link to="/news">
                        Show More <FontAwesomeIcon icon={faChevronRight} />
                    </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}