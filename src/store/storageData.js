import { Base64 } from 'js-base64';

export default function getData(item) {
    const dataString = localStorage.getItem(item);
    let decode = Base64.decode(dataString);
    const userData = JSON.parse(decode);
    return userData
}

export function saveData (item, value) {
    let valueString = JSON.stringify(value);
    let encode = Base64.encode(valueString);
    localStorage.setItem(item, encode);
};

export function removeData (item) {
  localStorage.removeItem(item)
};