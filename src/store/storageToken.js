export default function getToken() {
  const tokenString = localStorage.getItem('token');
  const tokenStringType = localStorage.getItem('tokenType');
  const userToken = JSON.parse(tokenString);
  const userTokenType = JSON.parse(tokenStringType);
  return userTokenType + ' ' + userToken
}

export function checkToken () {
  const tokenString = localStorage.getItem('token');
  const userToken = JSON.parse(tokenString);
  return userToken
};

export function saveToken (userToken, userTokenType) {
  localStorage.setItem('token', JSON.stringify(userToken));
  localStorage.setItem('tokenType', JSON.stringify(userTokenType));
};

export function removeToken () {
  localStorage.removeItem('token')
  localStorage.removeItem('tokenType')
};