Bootstrap.scss -> _variable.scss:

- Ubah variabel warning menjadi orange dengan hexa #f78a28
- Ubah variabel secondary menjadi grey-900 dengan hexa #1a1a1a


SETUP REPOSITORI PERTAMA KALI
- git clone (paste http clone)
- lakukan "npm install"
- jalankan aplikasi dengan "npm run start"

STEP DEVELOPING
- ke bitbucket, login, masuk ke repositori kamu
- lakukan sync tinggal klik sync, kalo tidak ada sync berarti tidak ada update
- di lokal pastikan posisi di brnach master, kalo belum lakukan "git checkout master"
- lakukan "git pull origin master"
- lakukan develop
- kalo sudah selesai lakukan "git add .", tapi pastikan posisi di branch master
- lakukan commit "git commit -m "isi pesan commit"
- kalo pake vscode bisa gunakan source control, tinggal klik tanda "+" untuk git add kemudian klik checklist untuk commit
- pastikan sudah tercommit semua dengan cara cek git status
- push ke bitbucket dengan lakukan "git push origin master"
- ke bitbucket, login, masuk ke repositori kamu
- pilih pull request, klik pull request (lihat tutorial yg dikirim di group WA)

FILE ENV
    saat ini kita fokus ke frontend dulu, jadi untuk backendnya kita manfaatkan backend yang sudah disetting diserver untuk tujuan testing ya.
Caranya sesuaikan isi dari file .env seperti yang dikirim di group WA, kurang lebih isinya seperti ini
    REACT_APP_API="https://apitesting.cakrawalabahasa.com/api/"
    REACT_APP_PUBLIC="https://admin.cakrawalabahasa.com"
    REACT_APP_DEBUG=true